FROM selenium/node-chrome:3.141.59-xenon
LABEL authors=SeleniumHQ
# USER seluser
USER root

# Scripts to run Selenium Standalone
COPY start-selenium-standalone.sh /opt/bin/start-selenium-standalone.sh
# Supervisor configuration file
COPY selenium.conf /etc/supervisor/conf.d/

ARG APP_FOLDER=/src
WORKDIR $APP_FOLDER
COPY --chown=root:root . $APP_FOLDER

RUN apt-get update && \
    apt-get install -y nodejs \
    npm

RUN npm install \
   && npm update \
   && npm install -g webdriver-manager \
   && npm install -g protractor \
   && webdriver-manager update 
   
ADD /resources/packages/protractor-automation-helper $APP_FOLDER/node_modules/protractor-automation-helper

WORKDIR $APP_FOLDER
RUN protractor protractor.conf.js

EXPOSE 4444
CMD ["npm", "run", "start"]#  