'use strict';
const setupUtilities = require('./setup-utilities');
const { getParam, toBoolean } = setupUtilities;
const { MAX_INSTANCES, HEADLESS_BROWSER, MAX_SESSIONS } = process.env;

const maxSessions = MAX_SESSIONS || getParam(1, '--params.maxSessions', false);
const maxBrowserInstances = MAX_INSTANCES || getParam(1, '--params.maxInstances', false);
const useHeadlessBrowser = HEADLESS_BROWSER || toBoolean(getParam(true, '--params.headlessBrowser', false));
const chromeHeadlessArgs =
    ['--headless',
        '--disable-gpu',
        '--window-size=1280x800',
        '--disable-dev-shm-usage',
        '--no-sandbox',
        '--acceptInsecureCerts',
        '--disable-infobars',
        '--ignore-certificate-errors',
        '--disable-blink-features=BlockCredentialedSubresources',
        '--disable-web-security'];

const chromeOptions = {
    args: useHeadlessBrowser ? chromeHeadlessArgs : [ '--disable-infobars' ],
    prefs: {
        download: {
            prompt_for_download: false,
            directory_upgrade: true,
            default_directory: 'Downloads',
        },
    },
    perfLoggingPrefs: {
        enableNetwork: true,
        enablePage: false,
    },
};
const loggingPrefs = {
    performance: 'ALL',
    browser: 'ALL',
};
const configSetup = {
    restartBrowserBetweenTests: false,
    SELENIUM_PROMISE_MANAGER: false,
    multiCapabilities: [{
        loggingPrefs,
        browserName: 'chrome',
        chromeOptions: chromeOptions,
        shardTestFiles: 'true',
        maxInstances: maxBrowserInstances,
        acceptInsecureCerts: true,
    }],
    allScriptsTimeout: 300000,
    suites: {
        health_tests: './e2e/test-suites/health-check-test-suite/**/*.e2e-spec.ts',
    },
    capabilities: {
        loggingPrefs,
        browserName: 'chrome',
        chromeOptions: chromeOptions,
        acceptInsecureCerts: true,
    },
    seleniumAddress: {
        local: 'http://localhost:4723/wd/hub',
    },
    params: {
        maxSessions: maxSessions,
        maxInstances: 5,
        users: {
            admin: {
                username: 'foodee_k1qphx62',
                password: '111',
            },
        },
    },
    baseUrl: 'https://gqa.test.now.vn',
    framework: 'jasmine',
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 300000,
        print: function() {
        },
    },
};
module.exports = configSetup;
