import { browser } from 'protractor';

import { User, UserType } from '../../page-objects/pages/models/user.model';

export class CredentialsHelper {
    private static readonly users = browser.params.users;

    static readonly admin: User = {
        username: CredentialsHelper.users.admin.username,
        password: CredentialsHelper.users.admin.password,
        userType: UserType.ADMIN,
    };
}
