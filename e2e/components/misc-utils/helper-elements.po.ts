import { By } from 'protractor';

import { CommonPageConstant } from '../../page-objects/pages/common/common-page.constant';

import { HtmlHelper } from './html-helper';

const { additionalAttributes } = HtmlHelper;
const { imageIdentifiers } = CommonPageConstant;

export class HelperElementsPageObjects {

    static get image() {
        return By.css('img');
    }

    static get precedingParent() {
        return By.xpath(`(../../preceding-sibling::div[@${additionalAttributes.fullpath}])[last()]`);
    }

    static get menuCollapseSelector() {
        return By.xpath(`//img[contains(@src,"${imageIdentifiers.collapsed}")]`);
    }

    static get menuExpandSelector() {
        return By.xpath(`//img[contains(@src,"${imageIdentifiers.collapsed}")]`);
    }
}
