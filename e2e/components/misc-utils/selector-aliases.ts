import { By, Locator } from 'protractor';
import { ComponentHelpersFactory } from 'protractor-automation-helper';

import { DfElement, DfElements } from '../misc-utils/df-elements-helper';
import { HtmlHelper } from '../misc-utils/html-helper';

const { additionalAttributes } = HtmlHelper;

export function $(locator: Locator, name: string) {
    return new DfElement(locator, name);
}

export function $linkText(text: string, parentText: string) {
    const selector =
        `//*[${ComponentHelpersFactory.getXPathFunctionForStringComparison(parentText, `@${additionalAttributes.fullpath}`)}]
        //following-sibling::div/div[(a[contains(@class,"clsTreeLeaf")] or contains(@class,"clsTreeLeafDiv")
        or contains(@class,"clsTreeLeafMouseOver")
        or contains(@class,"clsTreeLeafDivSelected")) and ${ComponentHelpersFactory.getXPathFunctionForDot(text)}]//a`;

    return $(By.xpath(selector), text);
}

export function $fullPath(text: string) {
    return new DfElement(By.xpath(`//div[(contains(@class,"clsTreeContainerNode") or contains(@class,"clsTreeNode"))
        and ${ComponentHelpersFactory.getXPathFunctionForStringComparison(text, `@${additionalAttributes.fullpath}`)}]`), text);
}

export function $$(locator: Locator, name: string) {
    return new DfElements(locator, name);
}
