export class CommonPageConstant {
    static imageIdentifiers = {
        collapsed: 'collapsed',
        expanded: 'expanded',
    };

    static readonly  attributes = {
        ids: {
            sample: 'sample',
        },
        labels: {
            sample: 'sample',
        },
    };
}
