import { AlertHelper } from '../../../components/html/alert-helper';
import { WaitHelper } from '../../../components/html/wait-helper';
import { ExpectationHelper } from '../../../components/misc-utils/expectation-helper';

import { CommonPage } from './common.po';
import { StepLogger } from "../../../../core/logger/step-logger";

export class CommonPageHelper {

    /**
     * Verify alert message and accept
     */
    public static async verifyAlertMessageAndAccept(message: string) {
        await ExpectationHelper.verifyAlertMessage(message);
        await AlertHelper.acceptAlert();
    }

    /**
     * Verify alert message absent
     */
    public static async verifyAlertMessageAbsent() {
        await ExpectationHelper.verifyAlertMessageAbsent();
    }

    /**
     * Wait for all type of loading icons displayed
     */
    public static async waitForAllLoadingIconsDisappeared() {
        StepLogger.subStep('Wait all types of loading icon disappeared');
        await WaitHelper.waitForElementToBeHidden(CommonPage.loadingButtons.loadingIcon.item);
        await WaitHelper.waitForElementToBeHidden(CommonPage.loadingButtons.loadingMicrosite.item);
        await WaitHelper.waitForElementToBeHidden(CommonPage.loadingButtons.loadingNowLoadingCircle.item);
        await WaitHelper.waitForElementToBeHidden(CommonPage.loadingButtons.faSpin.item);
        await WaitHelper.waitForElementToBeHidden(CommonPage.loadingButtons.resetAnimateIcon.item);
    }

    /**
     * Wait for Page to be stable
     */
    public static async waitForPageToBeStable() {
        await this.waitForAllLoadingIconsDisappeared();
        StepLogger.subStep('Wait page to stable');
        await WaitHelper.waitForPageToStable();
    }

    /**
     * Wait for all type of loading icons displayed and Page to be stable
     */
    public static async waitForAllLoadingIconsDisappearedAndPageToBeStable() {
        await this.waitForAllLoadingIconsDisappeared();
        await this.waitForPageToBeStable();
    }
}
