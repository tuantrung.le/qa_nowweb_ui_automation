import { By } from 'protractor';

import { HtmlHelper } from '../../../components/misc-utils/html-helper';
import { IDfElement } from '../../../components/misc-utils/i-df-element';
import { $ } from '../../../components/misc-utils/selector-aliases';
import { xpath } from '../../../components/misc-utils/xpath-builder';

export class CommonPage {

    static get loadingButtons() {
        return {
            get loadingIcon() {
                return $(By.css('.loading-center-absolute'), 'Loading');
            },
            get loadingMicrosite() {
                return $(By.css('.loading-microsite'), 'Loading-microsite');
            },
            get loadingNowLoadingCircle() {
                return $(By.css('.now-loading'), 'nowloading');
            },
            get boxLoading() {
                return $(By.css('.box-loading'), 'box-loading');
            },
            get faSpin() {
                return $(By.css('.fa-spin'), 'fa-spin');
            },
            get resetAnimateIcon() {
                return $(By.css('.rst-animate-tip'), 'rst-animate-tip');
            },
        };
    }

    static getElementByPlaceHolder(value: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.placeholder}='${value}']`),
            name);
    }

    static getElementByTextAndHref(href: string, text: string): IDfElement {
        return $(xpath()
            .contains(HtmlHelper.attributes.href, href)
            .text(text)
            .buildByObject(), text);
    }

    static getElementByContainsTextAndHref(href: string, text: string): IDfElement {
        return $(xpath()
            .contains(HtmlHelper.attributes.href, href)
            .textContains(text)
            .buildByObject(), text);
    }

    static getElementByIdEndsWith(idValue: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.id}$='${idValue}']`), name);
    }

    static getElementByIdContains(idValue: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.id}*='${idValue}']`), name);
    }

    static getElementByIdStartsWith(idValue: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.id}^='${idValue}']`), name);
    }

    static getElementByNameStartsWith(nameValue: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.name}^='${nameValue}']`), name);
    }

    static getElementByNameContains(nameValue: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.name}*='${nameValue}']`), name);
    }

    static getElementByNameEndsWith(nameValue: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.name}$='${nameValue}']`), name);
    }

    static getElementByClassContains(className: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.class}*='${className}']`), name);
    }

    static getElementByClassEndsWith(className: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.class}$='${className}']`), name);
    }

    static getElementByClassStartsWith(className: string, name: string): IDfElement {
        return $(By.css(`[${HtmlHelper.attributes.class}^='${className}']`), name);
    }
}
