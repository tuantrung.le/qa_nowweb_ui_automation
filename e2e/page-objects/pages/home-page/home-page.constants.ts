export class HomePageConstant {

    static readonly elementNames = {
        searchTextBox: 'Search textbox',
        now: 'Now',
        userDropdownMenu: 'User dropdown menu',
        logOut: 'Log out',
        logIn: 'Log in',
    };
}
