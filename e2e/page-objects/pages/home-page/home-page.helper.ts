import { StepLogger } from '../../../../core/logger/step-logger';
import { HomePage } from './home-page.po';
import { ExpectationHelper } from '../../../components/misc-utils/expectation-helper';
import { CommonPageHelper } from '../common/common-page.helper';
import { PageHelper } from "../../../components/html/page-helper";

export class HomePageHelper {

    static async verifyHomePageDisplayed() {
        StepLogger.subVerification('Verify homepage displayed');
        await ExpectationHelper.verifyDisplayedStatus(HomePage.textbox.searchTextBox);
    }

    static async clickOnTopNowIcon() {
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
        StepLogger.subStep('Click on top button');
        await PageHelper.click(HomePage.element.topLeftNowIcon);
    }

    static async clickOnLogoutFromUserDropdownMenu() {
        StepLogger.subStep('Click on User dropdown');
        await PageHelper.click(HomePage.userMenu.userDropdown);
        StepLogger.subStep('Click on logout option');
        // await WaitHelper.waitForElementToBeDisplayed(HomePage.userMenu.logOut.item);
        await PageHelper.click(HomePage.userMenu.logOut);
    }

    static async clickOnLogoutFromUserDropdownMenuAndVerifyUserLoggedOut() {
        StepLogger.postCondition('-------------Post condition: Logout---------------');
        await this.clickOnLogoutFromUserDropdownMenu();
        await this.verifyUserLoggedOut();
    }

    static async verifyUserLoggedOut() {
        StepLogger.subVerification('Verify User Logged out & login button displayed');
        await ExpectationHelper.verifyDisplayedStatus(HomePage.userMenu.logIn);
    }
}
