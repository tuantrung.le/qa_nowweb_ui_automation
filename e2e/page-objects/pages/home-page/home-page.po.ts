import { By } from 'protractor';

import { $ } from '../../../components/misc-utils/selector-aliases';
import { HomePageConstant } from './home-page.constants';

const { elementNames: eNames } = HomePageConstant;

export class HomePage {

    static get textbox() {
        return {
            get searchTextBox() {
                return $(By.css('#delivery-footer-logo'), eNames.searchTextBox);
            },
        };
    }

    static get element() {
        return {
            get topLeftNowIcon() {
                return $(By.css(`[alt='${eNames.now}']`), 'Now Icon');
            },
        };
    }

    static get userMenu() {
        return {
            get userDropdown() {
                return $(By.id('user-dropdown'), eNames.userDropdownMenu);
            },
            get logOut() {
                return $(By.css('.icon-user-logout'), eNames.logOut)
            },
            get logIn() {
                return $(By.css('.btn-login'), eNames.logIn)
            }
        };
    }
}
