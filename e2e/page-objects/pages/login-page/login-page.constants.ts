export class LoginPageConstant {

    static readonly elementNames = {
        username: 'Username or email',
        password: 'Password',
        login: 'Log in',
        submit: 'Submit',
    };
}
