import { CommonPageHelper } from '../common/common-page.helper';
import { User } from '../models/user.model';
import { LoginPage } from './login-page.po';
import { StepLogger } from '../../../../core/logger/step-logger';
import { BasePageHelper } from "../base-page.helper";
import { PageHelper } from "../../../components/html/page-helper";
import { TextBoxHelper } from "../../../components/html/textbox-helper";

export class LoginPageHelper extends BasePageHelper {
    url = '/login';

    static async enterLoginInformation(user: User) {
        await this.clickOnLoginButton();
        await this.inputUserNameAndPassword(user);
        await this.clickOnSubmitButton();

    }

    static async clickOnLoginButton() {
        StepLogger.subStep('Click on Login button');
        await PageHelper.click(LoginPage.buttons.login);
    }

    static async clickOnSubmitButton() {
        StepLogger.subStep('Click on Submit button');
        await PageHelper.click(LoginPage.buttons.submit);
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
    }

    static async inputUserNameAndPassword(user: User) {
        StepLogger.subStep('Input Username');
        await TextBoxHelper.sendKeys(LoginPage.loginForm.username, user.username);
        StepLogger.subStep('Input Password');
        await TextBoxHelper.sendKeys(LoginPage.loginForm.password, user.password);
    }
}
