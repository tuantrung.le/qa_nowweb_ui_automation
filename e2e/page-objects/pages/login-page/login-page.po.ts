import { By } from 'protractor';

import { $ } from '../../../components/misc-utils/selector-aliases';

import { LoginPageConstant } from './login-page.constants';

const { elementNames: eNames } = LoginPageConstant;

export class LoginPage {

    static get loginForm() {
        return {
            get username() {
                return $(By.css(`[placeholder="${eNames.username}"]`), eNames.username);
            },
            get password() {
                return $(By.css(`[placeholder="${eNames.password}"]`), eNames.password);
            },
        };
    }

    static get buttons() {
        return {
            get login() {
                return $(By.css('.btn-login'), eNames.login);
            },
            get submit() {
                return $(By.css('.btn-submit'), eNames.submit);
            },
        };
    }

}
