export class RestaurantPageConstant {

    static readonly elementNames = {
        placeYourOrder: 'Place Your Order',
        placeYourOrderVN: 'Đặt hàng',
        okButtonOnPopup: 'Ok button on popup',
        submitOrder: 'Submit Order',
        waitForConfirmation: 'Wait for confirmation',
        reset: 'Reset',
        orderedItems: 'Selected items',
        searchTextBox: 'Search text box',
    };

    static readonly messages = {
        orderSuccessfully: 'Order Successfully',
        orderSuccessfullyVN: 'Đặt hàng thành công',
    };

    static readonly restaurantNames = {
        highlands: 'Highlands Coffee - Thương xá Tax',
        gongcha: `Trà Sữa Gong Cha - Nguyễn Đình Chiểu`,
    };

    static readonly restaurantDishes = {
        gongcha: `Lục trà chanh dây (M)`,
    };
}
