import { StepLogger } from '../../../../core/logger/step-logger';
import { ExpectationHelper } from '../../../components/misc-utils/expectation-helper';
import { RestaurantPage } from './restaurant-page.po';
import { WaitHelper } from '../../../components/html/wait-helper';
import { CommonPageHelper } from '../common/common-page.helper';
import { PageHelper } from '../../../components/html/page-helper';
import { TextBoxHelper } from "../../../components/html/textbox-helper";
import { ElementHelper } from "../../../components/html/element-helper";

export class RestaurantPageHelper {

    static async verifyRestaurantPageDisplayed() {
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
        StepLogger.subVerification('Verify Place your order button displayed');
        await ExpectationHelper.verifyDisplayedStatus(RestaurantPage.buttons.placeYourOrder);
    }

    static async resetCurrentDishesAndAddNewDishWithoutToppings(dish: string) {
        StepLogger.subStep(`Click on Reset button and add button for dish: ${dish}`);
        await this.clickOnClearDishesButton();
        StepLogger.subStep('Search and Add dishes');
        await this.searchAndAddDishes(dish);
    }

    static async clickOnClearDishesButton() {
        StepLogger.subStep('Click on Clear dishes (Xóa) button');
        await PageHelper.clickIfDisplayed(RestaurantPage.buttons.reset);
    }

    static async clickOnPlaceOrderButtonAndWaitForLoading() {
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
        StepLogger.subStep('Click on Place your order (Đặt hàng) button');
        await RestaurantPage.buttons.placeYourOrder.scrollToElement();
        await PageHelper.click(RestaurantPage.buttons.placeYourOrder);
        StepLogger.subStep('Wait for confirmation popup displayed with Google map completely');
        await WaitHelper.waitForElementToBeDisplayed(RestaurantPage.googleMap.lastItem.item);
    }

    static async verifyDishSelected(dish: string) {
        await WaitHelper.waitForElementToBeDisplayed(RestaurantPage.orderedItems.item);
        StepLogger.subVerification(`Verify dish ${dish} selected`);
        await ExpectationHelper.verifyDisplayedStatus(RestaurantPage.getSelectedDishByName(dish));
    }

    static async searchAndAddDishes(dish: string) {
        StepLogger.subStep('Wait for menu to be displayed');
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
        StepLogger.subStep(`Enter dish name to search box as ${dish}`);
        await TextBoxHelper.sendKeys(RestaurantPage.textbox.searchDish, dish);
        StepLogger.subStep(`Verify dish: ${dish} is listed in search result`);
        await ExpectationHelper.verifyDisplayedStatus(RestaurantPage.getDishInRestaurantList(dish));
        StepLogger.subStep(`Click on Add button for dish ${dish}`);
        await ElementHelper.waitAndClickButton(RestaurantPage.getAddButtonByDish(dish));
        StepLogger.subStep('Wait for item added - Card state button counted greater than zero');
        await WaitHelper.waitForElementToBeHidden(RestaurantPage.buttons.zeroCardState.item);
    }

    static async clickOkButtonOnAddDishPopup() {
        StepLogger.subStep('Click on Add button');
        await PageHelper.click(RestaurantPage.buttons.okInPopup);
    }

    static async verifySubmitOrderButtonDisplayed() {
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
        StepLogger.subVerification('Verify Submit order button displayed');
        await ExpectationHelper.verifyDisplayedStatus(RestaurantPage.buttons.submitOrder);
    }

    static async clickOnSubmitOrderButton() {
        StepLogger.subStep('Click on Place order button');
        await PageHelper.click(RestaurantPage.buttons.submitOrder);
    }

    static async verifyOrderSuccessfullyPopupDisplayed() {
        StepLogger.subVerification('Verify Order Submitted successfully popup displayed');
        await ExpectationHelper.verifyDisplayedStatus(RestaurantPage.popups.orderSubmittedSuccessfully);
    }

    static async clickOnWaitForConfirmationButton() {
        StepLogger.subStep('Click on Wait for confirmation button');
        await PageHelper.click(RestaurantPage.buttons.waitForConfirmation);
    }
}
