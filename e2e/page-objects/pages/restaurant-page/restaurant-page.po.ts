import { By } from 'protractor';

import { $ } from '../../../components/misc-utils/selector-aliases';
import { RestaurantPageConstant } from './restaurant-page.constants';
const { elementNames: eNames } = RestaurantPageConstant;

export class RestaurantPage {

    static get buttons() {
        return {
            get placeYourOrder() {
                return $(By.css('.now-bill-restaurant .btn-red'), eNames.placeYourOrderVN);
            },
            get okInPopup() {
                return $(By.css('#modal-topping .btn-red'), eNames.okButtonOnPopup);
            },
            get submitOrder() {
                return $(By.css('.submit-order'), eNames.submitOrder);
            },
            get waitForConfirmation() {
                return $(By.css('.btn-blue'), eNames.waitForConfirmation);
            },
            get reset() {
                return $(By.css('.btn-reset'), eNames.reset);
            },
            get zeroCardState() {
                return $(By.xpath(`//button[@class = 'cart-stats']/span[text() = '0']`), 'No-item-selected');
            },
            get backToTop() {
                return $(By.id('btn-back-top'), 'Back to top');
            }
        };
    }

    static get googleMap() {
        return {
            get lastItem() {
                return $(By.css('div.gmnoprint .gm-style-cc'), 'last-item-on-google-map');
            },
        };
    }

    static get textbox() {
        return {
            get searchDish() {
                return $(By.name('searchKey'), eNames.searchTextBox);
            },
        };
    }

    static get popups() {
        return {
            get orderSubmittedSuccessfully() {
                return $(By.xpath(`//p[@class = 'title-popup-order' and text() = '${RestaurantPageConstant.messages.orderSuccessfullyVN}']`),
                    RestaurantPageConstant.messages.orderSuccessfullyVN);
            },
        };
    }

    static get orderedItems() {
        return $(By.css('.order-card-item'), eNames.orderedItems);
    }

    static getAddButtonByDish(dish: string) {
        return $(By.xpath(`//div[@class  = 'row' and div/div[normalize-space(text()) = '${dish}']]//div[@class = "btn-adding"]`), dish);
    }

    static getDishInRestaurantList(dish: string) {
        return $(By.xpath(`//div[ @class ="item-restaurant-name" and text() = "${dish}"]`), dish);
    }

    static getSelectedDishByName(dish: string) {
        return $(By.xpath(`//span[@class = 'name-order' and text() = '${dish}']`), dish);
    }
}
