export class SearchPageConstant {

    static readonly elementNames = {
        searchTextBox: 'Search text box',
        searchButton: 'Search button',
        searchResultTable: 'Search result table',
        allRestaurants: 'All restaurants',
    };

    static readonly buttonNames = {
        all: 'All',
    };
}
