import { StepLogger } from '../../../../core/logger/step-logger';
import { SearchPage } from './search-page.po';
import { ExpectationHelper } from '../../../components/misc-utils/expectation-helper';
import { CommonPageHelper } from '../common/common-page.helper';
import { PageHelper } from '../../../components/html/page-helper';
import { TextBoxHelper } from "../../../components/html/textbox-helper";

export class SearchPageHelper {

    static async verifySearchPageDisplayed() {
        StepLogger.subVerification('Verify search page displayed');
        await ExpectationHelper.verifyDisplayedStatus(SearchPage.textbox.searchTextBox);
    }

    static async searchFoodByName(foodName: string) {
        StepLogger.subStep(`Input search text as ${foodName}`);
        await TextBoxHelper.sendKeys(SearchPage.textbox.searchTextBox, foodName);
        StepLogger.subStep('Click on search button');
        await PageHelper.click(SearchPage.buttons.searchButton);
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
    }

    static async verifySearchResultTableDisplayed(foodName: string) {
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
        StepLogger.subVerification('Verify search result table displayed');
        await ExpectationHelper.verifyDisplayedStatus(SearchPage.searchResultByRestaurantName(foodName));
    }

    static async clickSearchAllButton() {
        StepLogger.subStep('Click search all button');
        await PageHelper.click(SearchPage.buttons.searchAll);
    }

    static async verifyRestaurantsDisplayed() {
        await CommonPageHelper.waitForAllLoadingIconsDisappearedAndPageToBeStable();
        const numberOfRestaurants = await SearchPage.allRestaurantsList.item.count();
        StepLogger.subVerification('Verify list of all restaurant displayed');
        await ExpectationHelper.verifyValueGreaterOrEqualTo(numberOfRestaurants, 20, 'Restaurant number');
    }

    static async selectRestaurantByName(restaurantName: string) {
        StepLogger.subStep(`Select restaurant: ${restaurantName}`);
        await PageHelper.click(SearchPage.searchResultByRestaurantName(restaurantName));
        StepLogger.subStep('Switch to new tab');
        await PageHelper.switchToNewTabIfAvailable();
    }
}
