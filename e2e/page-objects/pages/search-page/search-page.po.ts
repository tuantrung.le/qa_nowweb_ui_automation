import { By } from 'protractor';

import { $, $$ } from '../../../components/misc-utils/selector-aliases';
import { SearchPageConstant } from './search-page.constants';
const { elementNames: eNames } = SearchPageConstant;

export class SearchPage {

    static get textbox() {
        return {
            get searchTextBox() {
                return $(By.css('#txtSearchHome'), eNames.searchTextBox);
            },
        };
    }

    static get buttons() {
        return {
            get searchButton() {
                return $(By.css('.btn-search'), eNames.searchButton);
            },
            get searchAll() {
                return $(By.xpath(`//span[contains(@class, 'category-item') and text() = '${SearchPageConstant.buttonNames.all}']`),
                        SearchPageConstant.buttonNames.all);
            }
        };
    }

    static get allRestaurantsList() {
        return $$(By.css('.item-restaurant'), eNames.allRestaurants);
    }

    static searchResultByRestaurantName(foodName: string) {
        return $(By.css(`[title='${foodName}']`), foodName);
    }
}
