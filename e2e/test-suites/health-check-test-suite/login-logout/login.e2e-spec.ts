import { StepLogger } from '../../../../core/logger/step-logger';
import { PageHelper } from '../../../components/html/page-helper';
import { CredentialsHelper } from '../../../components/misc-utils/credentials-helper';
import { LoginPageHelper } from '../../../page-objects/pages/login-page/login-page.helper';
import { SuiteNames } from '../../suite-name/suite-names';
import { HomePageHelper } from '../../../page-objects/pages/home-page/home-page.helper';

describe(SuiteNames.smokeTestCases, () => {
    let loginPageHelper: LoginPageHelper;
    const admin = CredentialsHelper.admin;

    beforeAll(async () => {
        await PageHelper.maximizeBrowser();
        loginPageHelper = new LoginPageHelper();
        await loginPageHelper.goTo();
    });

    it('Verify user can login and logout - [1001]', async () => {
        StepLogger.caseId = 1001;

        StepLogger.stepId(1);
        StepLogger.step('Enter login credentials');
        await LoginPageHelper.enterLoginInformation(admin);
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await HomePageHelper.verifyHomePageDisplayed();

        StepLogger.stepId(2);
        StepLogger.step('Click on logout menu');
        await HomePageHelper.clickOnLogoutFromUserDropdownMenu();
        StepLogger.verification('Verify user is logged out');
        await HomePageHelper.verifyUserLoggedOut();
    });
});
