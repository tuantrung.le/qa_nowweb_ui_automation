import { StepLogger } from '../../../../core/logger/step-logger';
import { PageHelper } from '../../../components/html/page-helper';
import { CredentialsHelper } from '../../../components/misc-utils/credentials-helper';
import { LoginPageHelper } from '../../../page-objects/pages/login-page/login-page.helper';
import { SuiteNames } from '../../suite-name/suite-names';
import { HomePageHelper } from '../../../page-objects/pages/home-page/home-page.helper';
import { SearchPageHelper } from '../../../page-objects/pages/search-page/search-page.helper';
import { RestaurantPageHelper } from '../../../page-objects/pages/restaurant-page/restaurant-page.helper';
import { RestaurantPageConstant } from "../../../page-objects/pages/restaurant-page/restaurant-page.constants";

describe(SuiteNames.smokeTestCases, () => {
    let loginPageHelper: LoginPageHelper;
    const admin = CredentialsHelper.admin;
    const gongchaRestaurant = RestaurantPageConstant.restaurantNames.gongcha;
    const gongchaDish = RestaurantPageConstant.restaurantDishes.gongcha;

    beforeAll(async () => {
        await PageHelper.maximizeBrowser();
        loginPageHelper = new LoginPageHelper();
        await loginPageHelper.goTo();
    });

    afterEach(async () => {
        await HomePageHelper.clickOnLogoutFromUserDropdownMenuAndVerifyUserLoggedOut();
    });

    it('Verify user can place an order at Gongcha - [1004]', async () => {
        StepLogger.caseId = 1004;

        StepLogger.stepId(1);
        StepLogger.step('Enter login credentials');
        await LoginPageHelper.enterLoginInformation(admin);
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await HomePageHelper.verifyHomePageDisplayed();

        StepLogger.stepId(2);
        StepLogger.step('Click on Now icon');
        await HomePageHelper.clickOnTopNowIcon();
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await SearchPageHelper.verifySearchPageDisplayed();

        StepLogger.stepId(3);
        StepLogger.step(`Search restaurant by name ${gongchaRestaurant}`);
        await SearchPageHelper.searchFoodByName(gongchaRestaurant);
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await SearchPageHelper.verifySearchResultTableDisplayed(gongchaRestaurant);

        StepLogger.stepId(4);
        StepLogger.step(`Click on restaurant icon ${gongchaRestaurant}`);
        await SearchPageHelper.selectRestaurantByName(gongchaRestaurant);
        StepLogger.verification('Verify Restaurant page displayed');
        await RestaurantPageHelper.verifyRestaurantPageDisplayed();

        StepLogger.stepId(5);
        const dish = gongchaDish;
        StepLogger.step(`Select dish as ${dish}`);
        await RestaurantPageHelper.resetCurrentDishesAndAddNewDishWithoutToppings(dish);
        StepLogger.verification('Verify dish selected');
        await RestaurantPageHelper.verifyDishSelected(dish);

        StepLogger.stepId(6);
        StepLogger.step('Click on Place your order button');
        await RestaurantPageHelper.clickOnPlaceOrderButtonAndWaitForLoading();
        StepLogger.verification('Verify Submit order button displayed');
        await RestaurantPageHelper.verifySubmitOrderButtonDisplayed();

        StepLogger.stepId(7);
        StepLogger.step('Click on Submit order button');
        await RestaurantPageHelper.clickOnSubmitOrderButton();
        StepLogger.verification('Verify Order Submitted successfully popup displayed');
        await RestaurantPageHelper.verifyOrderSuccessfullyPopupDisplayed();

        StepLogger.stepId(8);
        StepLogger.subStep('Click on wait for confirmation button');
        await RestaurantPageHelper.clickOnWaitForConfirmationButton();
        StepLogger.verification('Verify Restaurant page displayed');
        await RestaurantPageHelper.verifyRestaurantPageDisplayed();
    });
});
