import { StepLogger } from '../../../../core/logger/step-logger';
import { PageHelper } from '../../../components/html/page-helper';
import { CredentialsHelper } from '../../../components/misc-utils/credentials-helper';
import { LoginPageHelper } from '../../../page-objects/pages/login-page/login-page.helper';
import { SuiteNames } from '../../suite-name/suite-names';
import { HomePageHelper } from '../../../page-objects/pages/home-page/home-page.helper';
import { SearchPageHelper } from '../../../page-objects/pages/search-page/search-page.helper';
import { RestaurantPageConstant } from "../../../page-objects/pages/restaurant-page/restaurant-page.constants";

describe(SuiteNames.smokeTestCases, () => {
    let loginPageHelper: LoginPageHelper;
    const adminUser = CredentialsHelper.admin;

    beforeAll(async () => {
        await PageHelper.maximizeBrowser();
        loginPageHelper = new LoginPageHelper();
        await loginPageHelper.goTo();
    });

    afterEach(async () => {
        await HomePageHelper.clickOnLogoutFromUserDropdownMenu();
        await HomePageHelper.verifyUserLoggedOut();
    });

    it('Verify User can Search for restaurant: Highland coffee - [1002]', async () => {
        StepLogger.caseId = 1002;

        StepLogger.stepId(1);
        StepLogger.step('Enter login credentials');
        await LoginPageHelper.enterLoginInformation(adminUser);
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await HomePageHelper.verifyHomePageDisplayed();

        StepLogger.stepId(2);
        StepLogger.step('Click on Now icon');
        await HomePageHelper.clickOnTopNowIcon();
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await SearchPageHelper.verifySearchPageDisplayed();

        StepLogger.stepId(3);
        const highlands = RestaurantPageConstant.restaurantNames.highlands;
        StepLogger.step(`Search restaurant by name ${highlands}`);
        await SearchPageHelper.searchFoodByName(highlands);
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await SearchPageHelper.verifySearchResultTableDisplayed(highlands);
    });

    it('Verify user can search All the restaurants - [1003]', async () => {
        StepLogger.caseId = 1003;

        StepLogger.stepId(1);
        StepLogger.step('Enter login credentials');
        await LoginPageHelper.enterLoginInformation(adminUser);
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await HomePageHelper.verifyHomePageDisplayed();

        StepLogger.stepId(2);
        StepLogger.step('Click on Now icon');
        await HomePageHelper.clickOnTopNowIcon();
        StepLogger.verification('Reporting menu should appearing on left vertical pane.');
        await SearchPageHelper.verifySearchPageDisplayed();

        StepLogger.stepId(3);
        StepLogger.step('Search all restaurants');
        await SearchPageHelper.clickSearchAllButton();
        StepLogger.verification('Verify all restaurant displayed');
        await SearchPageHelper.verifyRestaurantsDisplayed();
    });
});
