export class SuiteNames {
    public static readonly smokeTestCases = 'Smoke Test Cases';
    public static readonly regressionTestCases = 'Regression Test Cases';
}
