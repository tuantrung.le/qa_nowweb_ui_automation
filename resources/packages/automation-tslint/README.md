# *tslint-aurea*
Custom Aurea tslint rules

## Structure
- All rules files should be inside `src` folder
- All rule files should end with `Rule` suffix
- Project should be compiled with `npm run build` command before commit
- Project can be published with `npm run publish` command

## Project should be used as dependency in package.json
"@aurea/aurea-automation-tslint": "~0.0.1"

## List of rules
"expectation-helper": true,
"no-element-outside-page-class": true,
"no-step-verification-in-helper": true,
"no-sub-step-verification-in-spec": true


## Confluence page
[TSlint rules](https://confluence.devfactory.com/display/ENQAT/TSlint+rules+for+protractor+typescript+PR+rules)
