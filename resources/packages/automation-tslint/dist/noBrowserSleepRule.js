"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var NodeUtils_1 = require("./utils/NodeUtils");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoBrowserSleepWalker(sourceFile, this.getOptions()));
    };
    Rule.BROWSER_SLEEP_EXCLUDED_FILES = [
        'page-helper.ts',
        'static-wait-helper.ts',
    ];
    Rule.PAGE_HELPER_FILE = 'page-helper.ts';
    Rule.BROWSER_SLEEP_FAILURE_STRING = 'Use of browser.sleep forbidden';
    Rule.BROWSER_SLEEP_EXPRESSION_TO_BE_SEARCHED = 'browser';
    Rule.BROWSER_SLEEP_EXPRESSION_NAME_TEXT = 'sleep';
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var NoBrowserSleepWalker = (function (_super) {
    __extends(NoBrowserSleepWalker, _super);
    function NoBrowserSleepWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoBrowserSleepWalker.prototype.visitPropertyAccessExpression = function (node) {
        if (!NodeUtils_1.isSourceFileMemberOfArr(node.getSourceFile().fileName, Rule.BROWSER_SLEEP_EXCLUDED_FILES)
            && node.expression.getText() === Rule.BROWSER_SLEEP_EXPRESSION_TO_BE_SEARCHED
            && node.name.getText() === Rule.BROWSER_SLEEP_EXPRESSION_NAME_TEXT) {
            this.addFailureAt(node.getStart(), node.getWidth(), Rule.BROWSER_SLEEP_FAILURE_STRING);
        }
        _super.prototype.visitPropertyAccessExpression.call(this, node);
    };
    return NoBrowserSleepWalker;
}(tslint_1.RuleWalker));
