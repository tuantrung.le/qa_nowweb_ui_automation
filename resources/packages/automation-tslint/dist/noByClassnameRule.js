"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoByClassnameWalker(sourceFile, this.getOptions()));
    };
    Rule.BY_CLASSNAME_FAILURE_STRING = 'Prefer using By.css over By.className when using more than one classname';
    Rule.BY_CLASSNAME_EXPRESSION_TO_BE_SEARCHED = 'By.className';
    Rule.BY_CLASSNAME_REPLACE_CSS = 'By.css';
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var NoByClassnameWalker = (function (_super) {
    __extends(NoByClassnameWalker, _super);
    function NoByClassnameWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoByClassnameWalker.prototype.visitCallExpression = function (node) {
        if (node.expression.getText() === Rule.BY_CLASSNAME_EXPRESSION_TO_BE_SEARCHED) {
            if (node.arguments.length > 0) {
                var idText = node.arguments[0].getText();
                var idTextWithoutQuotes = idText.slice(1, -1);
                if (this.isMultipleClassnameUsed(idTextWithoutQuotes)) {
                    var errorStart = node.getStart();
                    var errorEnd = errorStart
                        + Rule.BY_CLASSNAME_EXPRESSION_TO_BE_SEARCHED.length
                        + idText.length + 2;
                    var fix = tslint_1.Replacement.replaceFromTo(errorStart, errorEnd, Rule.BY_CLASSNAME_REPLACE_CSS + "('" + this.fixedClasses(idTextWithoutQuotes) + "')");
                    this.addFailureAt(node.getStart(), node.getWidth(), Rule.BY_CLASSNAME_FAILURE_STRING, fix);
                }
            }
        }
        _super.prototype.visitCallExpression.call(this, node);
    };
    NoByClassnameWalker.prototype.isMultipleClassnameUsed = function (inputClasses) {
        return inputClasses.split(' ').length > 1;
    };
    NoByClassnameWalker.prototype.fixedClasses = function (inputClasses) {
        var classes = inputClasses.split(' ');
        return classes
            .map(function (item) {
            return "." + item;
        }).join('');
    };
    return NoByClassnameWalker;
}(tslint_1.RuleWalker));
