"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoByXPathWalker(sourceFile, this.getOptions()));
    };
    Rule.CLASS_NAME_SPEC = 'po.ts';
    Rule.BY_XPATH_FAILURE_STRING = 'Prefer using By.css over By.xpath.';
    Rule.BY_XPATH_EXPRESSION_TO_BE_SEARCHED = 'By';
    Rule.BY_XPATH_EXPRESSION_NAME_TEXT = 'xpath';
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var NoByXPathWalker = (function (_super) {
    __extends(NoByXPathWalker, _super);
    function NoByXPathWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoByXPathWalker.prototype.visitPropertyAccessExpression = function (node) {
        if (node.getSourceFile().fileName.includes(Rule.CLASS_NAME_SPEC)
            && node.expression.getText() === Rule.BY_XPATH_EXPRESSION_TO_BE_SEARCHED
            && node.name.getText() === Rule.BY_XPATH_EXPRESSION_NAME_TEXT) {
            this.addFailureAt(node.getStart(), node.getWidth(), Rule.BY_XPATH_FAILURE_STRING);
        }
        _super.prototype.visitPropertyAccessExpression.call(this, node);
    };
    return NoByXPathWalker;
}(tslint_1.RuleWalker));
