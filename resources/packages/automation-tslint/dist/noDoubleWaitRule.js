"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoDoubleWaitWalker(sourceFile, this.getOptions()));
    };
    Rule.DOUBLE_WAIT_FAILURE_STRING = 'Code block uses double wait statement. Prefer using only Pagehelper method.';
    Rule.ERROR_PATTERNS = [' WaitHelper.getInstance().waitForElementToBeClickable', ' WaitHelper.waitForElementToBeClickable'];
    Rule.DOUBLE_WAIT_PAGEHELPER_CLICK_TEXT = 'PageHelper.click';
    Rule.DOUBLE_WAIT_AWAIT_TEXT = 'await';
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var NoDoubleWaitWalker = (function (_super) {
    __extends(NoDoubleWaitWalker, _super);
    function NoDoubleWaitWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoDoubleWaitWalker.prototype.visitBlock = function (node) {
        var _this = this;
        var statements = node.statements;
        var _loop_1 = function (i) {
            Rule.ERROR_PATTERNS.map(function (errorPattern) {
                if (statements[i].getText().includes(errorPattern)
                    && statements[i + 1].getText().includes(Rule.DOUBLE_WAIT_PAGEHELPER_CLICK_TEXT)) {
                    var errorStart = statements[i].getStart();
                    var errorEnd = statements[i].getText().length
                        + statements[i + 1].getText().length
                        + 2 * Rule.DOUBLE_WAIT_AWAIT_TEXT.length;
                    _this.addFailureAt(errorStart, errorEnd, Rule.DOUBLE_WAIT_FAILURE_STRING);
                }
            });
        };
        for (var i = 0; i < statements.length - 1; i += 1) {
            _loop_1(i);
        }
        _super.prototype.visitBlock.call(this, node);
    };
    return NoDoubleWaitWalker;
}(tslint_1.RuleWalker));
