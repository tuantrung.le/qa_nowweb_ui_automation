"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var CLASS_NAME_SPEC = 'po.ts';
var FAILURE_STRING_VERIFICATION = 'Elements with selectors should be inside po file';
var ERROR_PATTERNS = ['element', 'element.all'];
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoElementOutsidePageClassWalker(sourceFile, this.getOptions()));
    };
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var NoElementOutsidePageClassWalker = (function (_super) {
    __extends(NoElementOutsidePageClassWalker, _super);
    function NoElementOutsidePageClassWalker(sourceFile, options) {
        return _super.call(this, sourceFile, options) || this;
    }
    NoElementOutsidePageClassWalker.prototype.visitCallExpression = function (node) {
        var _this = this;
        if (!node.getSourceFile().fileName.includes(CLASS_NAME_SPEC)) {
            var text_1 = node.getText();
            ERROR_PATTERNS.map(function (errorPattern) {
                var REGEX = new RegExp(errorPattern + "\\(");
                var match = text_1.match(REGEX);
                if (match && node.arguments.length === 1) {
                    _this.addFailureAt(node.getStart() + match.index, errorPattern.length, FAILURE_STRING_VERIFICATION);
                }
            });
        }
        _super.prototype.visitCallExpression.call(this, node);
    };
    return NoElementOutsidePageClassWalker;
}(tslint_1.RuleWalker));
exports.NoElementOutsidePageClassWalker = NoElementOutsidePageClassWalker;
