"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoFirstSelectorWalker(sourceFile, this.getOptions()));
    };
    Rule.FIRST_SELECTOR_FAILURE_STRING = 'Finding element by first() or get(0) selector is not preferred. '
        + 'Please use xpath with index use xpath (//tagName)[<IndexHere>]';
    Rule.FIRST_SELECTOR_EXCLUDED_TEXT = ['first', 'get'];
    Rule.XPATH_BUILDER_CALLER_EXPRESSION = 'xpath';
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var NoFirstSelectorWalker = (function (_super) {
    __extends(NoFirstSelectorWalker, _super);
    function NoFirstSelectorWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoFirstSelectorWalker.prototype.visitCallExpression = function (node) {
        if (!this.isNodeUsingXpathBuilderAsCaller(node)
            && this.isNodeUsingBlockedSelectors(node, Rule.FIRST_SELECTOR_EXCLUDED_TEXT)) {
            this.addFailureAt(node.getStart(), node.getText().length, Rule.FIRST_SELECTOR_FAILURE_STRING);
        }
        _super.prototype.visitCallExpression.call(this, node);
    };
    NoFirstSelectorWalker.prototype.isNodeUsingXpathBuilderAsCaller = function (node) {
        return node.expression.getText().startsWith(Rule.XPATH_BUILDER_CALLER_EXPRESSION);
    };
    NoFirstSelectorWalker.prototype.isNodeUsingBlockedSelectors = function (node, blockedSelectors) {
        var nodeText = node.expression.getText();
        for (var _i = 0, blockedSelectors_1 = blockedSelectors; _i < blockedSelectors_1.length; _i++) {
            var blockedSelector = blockedSelectors_1[_i];
            if (nodeText.endsWith(blockedSelector)) {
                if (blockedSelector === 'get') {
                    var numberLiteral = Object.values(node.arguments)[0];
                    var arg = Number(numberLiteral.text);
                    if (arg === 0) {
                        return true;
                    }
                }
                else {
                    return true;
                }
            }
        }
        return false;
    };
    return NoFirstSelectorWalker;
}(tslint_1.RuleWalker));
