"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new NoStaticWaitWalker(sourceFile, this.getOptions()));
    };
    Rule.STATIC_WAIT_FAILURE_STRING = 'Use of StaticWait.waitForMillSec is forbidden';
    Rule.STATIC_WAIT_EXPRESSION_TO_BE_SEARCHED = 'StaticWait.waitForMillSec';
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var NoStaticWaitWalker = (function (_super) {
    __extends(NoStaticWaitWalker, _super);
    function NoStaticWaitWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NoStaticWaitWalker.prototype.visitCallExpression = function (node) {
        if (node.expression.getText() === Rule.STATIC_WAIT_EXPRESSION_TO_BE_SEARCHED) {
            this.addFailureAt(node.getStart(), node.getWidth(), Rule.STATIC_WAIT_FAILURE_STRING);
        }
        _super.prototype.visitCallExpression.call(this, node);
    };
    return NoStaticWaitWalker;
}(tslint_1.RuleWalker));
