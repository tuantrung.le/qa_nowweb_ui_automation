"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isStringWithQuotes(inputText) {
    var singleQuote = "'";
    var doubleQuote = '"';
    var startingCharac = inputText[0];
    var endingCharac = inputText.slice(-1);
    return (startingCharac === singleQuote && endingCharac === singleQuote)
        || (startingCharac === doubleQuote && endingCharac === doubleQuote);
}
exports.isStringWithQuotes = isStringWithQuotes;
function isConcatenation(idValue) {
    var concatenationOperator = '+';
    return idValue.includes(concatenationOperator);
}
exports.isConcatenation = isConcatenation;
function isSourceFileMemberOfArr(sourceFileName, filesArr) {
    for (var _i = 0, filesArr_1 = filesArr; _i < filesArr_1.length; _i++) {
        var fileItem = filesArr_1[_i];
        if (sourceFileName.includes(fileItem)) {
            return true;
        }
    }
    return false;
}
exports.isSourceFileMemberOfArr = isSourceFileMemberOfArr;
