"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var tslint_1 = require("tslint");
var Rule = (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new VerifyMethodMissingWalker(sourceFile, this.getOptions()));
    };
    Rule.SPEC_FILE_FORMAT = 'spec.ts';
    Rule.VERIFY_METHOD_MISSING_FAILURE_STRING = 'Logger verfication method should be followed by verify method implemented in helper files.';
    Rule.VERIFY_METHOD_MISSING_STEP_TEXT = ['stepLogger.verification', 'logger.verification', 'StepLogger.verification', 'Logger.verification'];
    Rule.VERIFY_METHOD_MISSING_VERIFY_TEXT = 'verify';
    Rule.VERIFY_METHOD_MISSING_VALID_NEXT_STEP_TEXT = '.stepId';
    return Rule;
}(tslint_1.Rules.AbstractRule));
exports.Rule = Rule;
var VerifyMethodMissingWalker = (function (_super) {
    __extends(VerifyMethodMissingWalker, _super);
    function VerifyMethodMissingWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VerifyMethodMissingWalker.prototype.visitBlock = function (node) {
        if (node.getSourceFile().fileName.includes(Rule.SPEC_FILE_FORMAT)) {
            var statements = node.statements;
            for (var i = 0; i < statements.length - 1; i += 1) {
                if (this.isVerifyMissing(statements[i], statements[i + 1])) {
                    var errorStart = statements[i + 1].getStart();
                    var errorEnd = statements[i + 1].getText().length;
                    this.addFailureAt(errorStart, errorEnd, Rule.VERIFY_METHOD_MISSING_FAILURE_STRING);
                }
            }
        }
        _super.prototype.visitBlock.call(this, node);
    };
    VerifyMethodMissingWalker.prototype.isVerifyMissing = function (statement, nextStatement) {
        return Rule.VERIFY_METHOD_MISSING_STEP_TEXT.some(function (substring) { return statement.getText().includes(substring); })
            && !(nextStatement.getText().includes(Rule.VERIFY_METHOD_MISSING_VALID_NEXT_STEP_TEXT)
                || nextStatement.getText().includes(Rule.VERIFY_METHOD_MISSING_VERIFY_TEXT));
    };
    return VerifyMethodMissingWalker;
}(tslint_1.RuleWalker));
