# Protractor automation helper

[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

This is a collection of tools to help in the test implementation of easier projects.

### Usage

Just add the dependency to your project:

```bash
# Install the library in your project. That's all!
npm install protractor-automation-helper --save-dev
```

### Library Features

 - **[RollupJS](https://rollupjs.org/)** for multiple optimized bundles following the [standard convention](http://2ality.com/2017/04/setting-up-multi-platform-packages.html) and [Tree-shaking](https://alexjoverm.github.io/2017/03/06/Tree-shaking-with-Webpack-2-TypeScript-and-Babel/).
 - Tests, coverage and interactive watch mode using **[Jest](http://facebook.github.io/jest/)**
 - **[Prettier](https://github.com/prettier/prettier)** and **[TSLint](https://palantir.github.io/tslint/)** for code formatting and consistency.
 - **Docs automatic generation and deployment** to `gh-pages`, using **[TypeDoc](http://typedoc.org/)**
 - Automatic types `(*.d.ts)` file generation

### NPM scripts

 - `npm t`: Run test suite
 - `npm start`: Runs `npm run build` in watch mode
 - `npm run test:watch`: Run test suite in [interactive watch mode](http://facebook.github.io/jest/docs/cli.html#watch)
 - `npm run test:prod`: Run linting and generate coverage
 - `npm run build`: Generate bundles and typings, create docs
 - `npm run lint`: Lints code
 - `npm pack`: Packs the library in order to be used from another project without published (you need to put the reference in package.json for example as: "easier-test": "file:../easier-test/easier-test-1.0.0.tgz")
 - `npm publish`: Publish the library in the Nexus repo (you need to have permissions for this). Remember to create a Github release associated.

### Notes

This project is seeded from https://github.com/alexjoverm/typescript-library-starter.
Please check out here also for information: https://dev.to/alexjoverm/write-a-library-using-typescript-library-starter

### Lib documentation
https://trilogy-group.github.io/protractor-automation-helper/ 
