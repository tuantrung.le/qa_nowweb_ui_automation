export declare class ComponentHelpersFactory {
    /**
     * Returns contains(text(),@param) or text()=@param based on the parameter
     *
     * @example
     * // Returns contains('.',attributeValue)
     * this.getXPathFunctionForStringComparison(text:attributeValue, attribute:'.', isContains:true);
     * // Returns normalize-space(.)=attributeValue
     * this.getXPathFunctionForStringComparison(text:attributeValue, attribute:'.', isContains:false);
     * @param {string} text
     * @param {string} attribute
     * @param {boolean} isContains
     * @returns {string} contains(text(),@param) or text()=@param based on the parameter
     */
    static getXPathFunctionForStringComparison(text: string, attribute: string, isContains?: boolean): string;
    static getXPathFunctionForDot(text: string, isContains?: boolean): string;
    static getXPathFunctionForText(text: string, isContains?: boolean): string;
}
