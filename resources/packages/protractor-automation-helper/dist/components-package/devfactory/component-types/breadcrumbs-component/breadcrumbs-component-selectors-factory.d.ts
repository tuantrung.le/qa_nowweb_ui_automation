export declare class BreadcrumbsComponentSelectorsFactory {
    static readonly selectorTag: string;
    static readonly linksXPath: string;
    static getBreadcrumbLinkXPath(label: string, isContains?: boolean): string;
}
