export declare class ButtonComponentSelectorsFactory {
    static readonly selectorTag: string;
    static getButtonByFormControlNameXpath(formControlName: string, insidePopup?: boolean): string;
    static getDropdownButtonByLabelXpath(label: string, isContains?: boolean, insidePopup?: boolean): string;
    static getDropdownButtonOptionsByLabelXpath(labelOfDropdownButton: string, labelOfOptionButton: string, isContains?: boolean): string;
    static getButtonByIconClassNameXpath(iconClass: string, insidePopup?: boolean): string;
    static getButtonByLabelXpath(label: string, isContains?: boolean, insidePopup?: boolean): string;
}
