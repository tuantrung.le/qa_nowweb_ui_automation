export declare class CalendarComponentSelectorsFactory {
    static readonly selector: string;
    static getCreatedEventByTextXPath(calendarTitle: string, eventTitle: string, isContains?: boolean): string;
    static getCalendarByTitleXPath(calendarTitle: string): string;
    static getCalendarHeaderButtonByClassNameXPath(calendarTitle: string, className: string): string;
    static getCalendarHeaderXPath(calendarTitle: string): string;
    static getCalendarPreviousButtonXPath(calendarTitle: string): string;
    static getCalendarNextButtonXPath(calendarTitle: string): string;
}
