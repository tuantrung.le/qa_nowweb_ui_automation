export declare class DropdownComponentSelectorsFactory {
    static readonly selector: string;
    static getControlXPath(label: string, isContains?: boolean, insidePopup?: boolean): string;
    static getSelectedOptionXPath(label: string, isContains?: boolean, insidePopup?: boolean): string;
    static getOptionsXPath(label: string, isContains?: boolean, insidePopup?: boolean): string;
    static getOptionXPath(label: string, optionLabel: string, isContains?: boolean, insidePopup?: boolean): string;
}
