export declare class LoaderComponentSelectorsFactory {
    static readonly selector: string;
    static getControlXPath(): string;
}
