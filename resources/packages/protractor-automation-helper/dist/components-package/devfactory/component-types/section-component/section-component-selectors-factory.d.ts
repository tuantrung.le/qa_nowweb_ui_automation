export declare class SectionComponentSelectorsFactory {
    static getControlXPath(title: string): string;
    static getIconXPath(title: string, iconClass: string): string;
}
