export declare class SidebarMenuComponentSelectorsFactory {
    static readonly selector: string;
    static sideBarSubMenuItemXPath(text: string): string;
}
