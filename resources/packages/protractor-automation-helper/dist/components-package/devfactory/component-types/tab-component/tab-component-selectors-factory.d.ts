export declare class TabComponentSelectorsFactory {
    static activeTabWithTextXPath(sectionName: string, tab: string): string;
}
