export declare class TooltipComponentSelectorsFactory {
    static readonly selector: string;
    static getTooltipXPath(message: string): string;
}
