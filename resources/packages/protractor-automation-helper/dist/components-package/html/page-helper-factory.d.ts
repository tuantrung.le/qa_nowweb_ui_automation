import { WebdriverBy } from "protractor/built/locators";
import { ProtractorBrowser } from "protractor";
/**
 * Page helper for general utility
 */
export declare class PageHelperFactory {
    private _browser;
    private _by;
    private waitHelperFactory;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy);
    /**
     * To maximize the this._browser window
     */
    maximizeWindow(): Promise<void>;
    /**
     * Sets window size
     * @param {number} width
     * @param {number} height
     */
    setWindowSize(width: number, height: number): Promise<void>;
    /**
     * Wrapper for executing javascript code
     * @param {string | Function} script
     * @param varAargs
     * @returns {promise.Promise<any>}
     */
    executeScript(script: string | Function, ...varAargs: any[]): Promise<{}>;
    /**
     * Wrapper to return an active element
     * @returns {any}

     public  async getFocusedElement() {
    return this._browser.driver.switchTo().activeElement()
  }*/
    /**
     * Switch to a new tab if this._browser has availability
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    switchToNewTabIfAvailable(): Promise<void>;
    /**
     * Gets html attribute value
     * @param {WebElementPromise} elem
     * @param {string} attribute
     * @returns {string} attribute value
     */
    getAttributeValue(elem: any, attribute: string): Promise<any>;
    /**
     * Click on element
     * @param {any} targetElement
     * @returns {any}
     */
    click(targetElement: any): Promise<any>;
    /**
     * Click on the element and wait for it to get hidden
     * @param {any} targetElement
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    clickAndWaitForElementToHide(targetElement: any): Promise<void>;
    /**
     * Gets promise for current url
     * @returns {any}
     */
    currentUrl(): Promise<string>;
}
