import { ProtractorBrowser } from "protractor";
import { WebdriverBy } from "protractor/built/locators";
export declare class WaitHelperFactory {
    private _browser;
    private _by;
    private readonly EC;
    private readonly timeoutMessage;
    private DEFAULT_TIMEOUT;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy);
    /**
     * Default timeout for promises
     * @type {number}
     */
    /**
     * Wait for an element to exist
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    waitForElement(targetElement: any, timeout?: number, message?: string): Promise<void>;
    /**
     * Wait for an element to display
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    waitForElementToBeDisplayed(targetElement: any, timeout?: number, message?: string): Promise<void>;
    /**
     * Wait for an element to hide
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     * @returns {any}
     */
    waitForElementToBeHidden(targetElement: any, timeout?: number, message?: string): Promise<void>;
    /**
     * Wait for an element to become clickable
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    waitForElementToBeClickable(targetElement: any, timeout?: number, message?: string): Promise<void>;
    /**
     * Common waiter
     * @param {Function} functionName
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     * @returns {Promise<void>}
     */
    private waiter;
}
