import { Capabilities } from "protractor";
export declare class BrowserUtilitiesFactory {
    private _capabilities;
    constructor(_capabilities: Capabilities);
    isIE10(): boolean;
    isFirefox(): boolean;
    isEdge(): boolean;
    isSafari(): boolean;
}
