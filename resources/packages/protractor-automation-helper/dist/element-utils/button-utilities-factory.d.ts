import { ProtractorBrowser, WebElementPromise } from "protractor";
import { WebdriverBy } from "protractor/built/locators";
export declare class ButtonUtilitiesFactory {
    private _browser;
    private _by;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy);
    getButtonById(elementId: string): WebElementPromise;
}
