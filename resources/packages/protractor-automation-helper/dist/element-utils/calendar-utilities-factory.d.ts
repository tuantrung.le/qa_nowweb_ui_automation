import { ProtractorBrowser, Ptor, WebElementPromise } from "protractor";
import { WebdriverBy } from "protractor/built/locators";
export declare class CalendarUtilitiesFactory {
    private _browser;
    private _by;
    private _protractor;
    private DF_DATEPICKER_ARROW;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy, _protractor: Ptor);
    getDateById(elementId: string): WebElementPromise;
    getTimeById(elementId: string): WebElementPromise;
    setDateById(elementId: string, value: string, direction?: string): void;
}
