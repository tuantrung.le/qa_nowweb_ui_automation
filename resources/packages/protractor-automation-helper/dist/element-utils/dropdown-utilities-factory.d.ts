import { ProtractorBrowser, ProtractorExpectedConditions, WebElementPromise } from "protractor";
import { WebdriverBy } from "protractor/built/locators";
export declare class DropdownUtilitiesFactory {
    private _browser;
    private _by;
    private _expectedConditions;
    private DF_SELECT_OPTIONS_LIST;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy, _expectedConditions: ProtractorExpectedConditions);
    openDropdownByCss(selector: string): void;
    openDropdownById(id: string): void;
    closeDropdownByCss(selector: string): void;
    closeDropdownById(id: string): void;
    getDropdownById(elementId: string): WebElementPromise;
    setDropdownOptionIndexById(elementId: string, optionIndex: number): void;
    setDropdownValueById(elementId: string, value: string): void;
    setMultiDropdownOptionIndexById(elementId: string, optionIndex: number): void;
    setDropdownValueByIdWithoutSync(elementId: string, value: string): void;
    private scrollIntoOption;
}
