import { ProtractorBrowser, Ptor, WebElementPromise } from "protractor";
import { WebdriverBy } from "protractor/built/locators";
export declare class InputUtilitiesFactory {
    private _browser;
    private _by;
    private _protractor;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy, _protractor: Ptor);
    getInputById(elementId: string): WebElementPromise;
    setInputById(elementId: string, value: string): void;
}
