"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ComponentHelpersFactory = /** @class */ (function () {
    function ComponentHelpersFactory() {
    }
    /**
     * Returns contains(text(),@param) or text()=@param based on the parameter
     *
     * @example
     * // Returns contains('.',attributeValue)
     * this.getXPathFunctionForStringComparison(text:attributeValue, attribute:'.', isContains:true);
     * // Returns normalize-space(.)=attributeValue
     * this.getXPathFunctionForStringComparison(text:attributeValue, attribute:'.', isContains:false);
     * @param {string} text
     * @param {string} attribute
     * @param {boolean} isContains
     * @returns {string} contains(text(),@param) or text()=@param based on the parameter
     */
    ComponentHelpersFactory.getXPathFunctionForStringComparison = function (text, attribute, isContains) {
        if (isContains === void 0) { isContains = false; }
        return isContains
            ? "contains(" + attribute + ",\"" + text + "\")"
            : "normalize-space(" + attribute + ")=\"" + text + "\"";
    };
    ComponentHelpersFactory.getXPathFunctionForDot = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getXPathFunctionForStringComparison(text, ".", isContains);
    };
    ComponentHelpersFactory.getXPathFunctionForText = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getXPathFunctionForStringComparison(text, "text()", isContains);
    };
    return ComponentHelpersFactory;
}());
exports.ComponentHelpersFactory = ComponentHelpersFactory;
//# sourceMappingURL=component-helpers-factory.js.map