"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_helpers_factory_1 = require("../../component-helpers/component-helpers-factory");
var BreadcrumbsComponentSelectorsFactory = /** @class */ (function () {
    function BreadcrumbsComponentSelectorsFactory() {
    }
    Object.defineProperty(BreadcrumbsComponentSelectorsFactory, "linksXPath", {
        get: function () {
            return "//" + this.selectorTag + "//span//a";
        },
        enumerable: true,
        configurable: true
    });
    BreadcrumbsComponentSelectorsFactory.getBreadcrumbLinkXPath = function (label, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this
            .linksXPath + "[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForText(label, isContains) + "]";
    };
    BreadcrumbsComponentSelectorsFactory.selectorTag = "df-breadcrumbs";
    return BreadcrumbsComponentSelectorsFactory;
}());
exports.BreadcrumbsComponentSelectorsFactory = BreadcrumbsComponentSelectorsFactory;
//# sourceMappingURL=breadcrumbs-component-selectors-factory.js.map