"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var html_helper_factory_1 = require("../../../misc-utils/html-helper-factory");
var model_component_selectors_factory_1 = require("../modal-component/model-component-selectors-factory");
var component_helpers_factory_1 = require("../../component-helpers/component-helpers-factory");
var ButtonComponentSelectorsFactory = /** @class */ (function () {
    function ButtonComponentSelectorsFactory() {
    }
    ButtonComponentSelectorsFactory.getButtonByFormControlNameXpath = function (formControlName, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this.selectorTag + "[@" + html_helper_factory_1.HtmlHelperFactory.attributes
            .formControlName + "='" + formControlName + "']";
    };
    ButtonComponentSelectorsFactory.getDropdownButtonByLabelXpath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //span[contains(@class,'df-button__content')\n        and\n        [" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForDot(label, isContains) + "]]//parent::button[@ngbdropdowntoggle]";
    };
    ButtonComponentSelectorsFactory.getDropdownButtonOptionsByLabelXpath = function (labelOfDropdownButton, labelOfOptionButton, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getDropdownButtonByLabelXpath(labelOfDropdownButton) + "\n        //following-sibling::div[@ngbdropdownmenu]//button[contains(@class,'dropdown-item')\n        and\n        " + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForText(labelOfOptionButton, isContains) + "]";
    };
    ButtonComponentSelectorsFactory.getButtonByIconClassNameXpath = function (iconClass, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this.selectorTag + "[i[contains(@class,'" + iconClass + "')]]";
    };
    ButtonComponentSelectorsFactory.getButtonByLabelXpath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this
            .selectorTag + "[span[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForDot(label, isContains) + "]]";
    };
    ButtonComponentSelectorsFactory.selectorTag = "button";
    return ButtonComponentSelectorsFactory;
}());
exports.ButtonComponentSelectorsFactory = ButtonComponentSelectorsFactory;
//# sourceMappingURL=button-component-selectors-factory.js.map