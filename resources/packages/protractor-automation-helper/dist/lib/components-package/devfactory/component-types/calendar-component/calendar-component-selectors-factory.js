"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_helpers_factory_1 = require("../../component-helpers/component-helpers-factory");
var section_component_selectors_factory_1 = require("../section-component/section-component-selectors-factory");
var CalendarComponentSelectorsFactory = /** @class */ (function () {
    function CalendarComponentSelectorsFactory() {
    }
    CalendarComponentSelectorsFactory.getCreatedEventByTextXPath = function (calendarTitle, eventTitle, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getCalendarByTitleXPath(calendarTitle) + "\n            //*[contains(@class,\"cal-event\") and contains(@class,\"event-type-event\")\n            and " + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForText(eventTitle, isContains) + "]";
    };
    CalendarComponentSelectorsFactory.getCalendarByTitleXPath = function (calendarTitle) {
        return "//" + this
            .selector + "[" + section_component_selectors_factory_1.SectionComponentSelectorsFactory.getControlXPath(calendarTitle) + "]";
    };
    CalendarComponentSelectorsFactory.getCalendarHeaderButtonByClassNameXPath = function (calendarTitle, className) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//button[contains(normalize-space(@class),'" + className + "')]";
    };
    CalendarComponentSelectorsFactory.getCalendarHeaderXPath = function (calendarTitle) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//app-cam-calendar-header";
    };
    CalendarComponentSelectorsFactory.getCalendarPreviousButtonXPath = function (calendarTitle) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//*[@mwlcalendarpreviousview]";
    };
    CalendarComponentSelectorsFactory.getCalendarNextButtonXPath = function (calendarTitle) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//*[@mwlcalendarnextview]";
    };
    CalendarComponentSelectorsFactory.selector = "app-calendar";
    return CalendarComponentSelectorsFactory;
}());
exports.CalendarComponentSelectorsFactory = CalendarComponentSelectorsFactory;
//# sourceMappingURL=calendar-component-selectors-factory.js.map