"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_helpers_factory_1 = require("../../component-helpers/component-helpers-factory");
var CheckboxComponentSelectorsFactory = /** @class */ (function () {
    function CheckboxComponentSelectorsFactory() {
    }
    CheckboxComponentSelectorsFactory.getControlXpath = function (label, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this
            .selector + "//span[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForText(label, isContains) + "]\n        //preceding-sibling::" + this.checkboxBoxIcon;
    };
    CheckboxComponentSelectorsFactory.getCheckboxByFormControlNameXpath = function (name) {
        return "//df-checkbox[@formcontrolname=\"" + name + "\"]" + this.checkboxBoxIcon;
    };
    CheckboxComponentSelectorsFactory.selector = "df-checkbox";
    CheckboxComponentSelectorsFactory.checkboxBoxIcon = "span[@class=\"df-checkbox__icon\"]/i";
    return CheckboxComponentSelectorsFactory;
}());
exports.CheckboxComponentSelectorsFactory = CheckboxComponentSelectorsFactory;
//# sourceMappingURL=checkbox-component-selectors-factory.js.map