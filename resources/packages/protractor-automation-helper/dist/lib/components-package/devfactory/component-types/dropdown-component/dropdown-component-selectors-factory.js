"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var model_component_selectors_factory_1 = require("../modal-component/model-component-selectors-factory");
var component_helpers_factory_1 = require("../../component-helpers/component-helpers-factory");
var DropdownComponentSelectorsFactory = /** @class */ (function () {
    function DropdownComponentSelectorsFactory() {
    }
    DropdownComponentSelectorsFactory.getControlXPath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //label[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForText(label, isContains) + "]\n        //following-sibling::" + this.selector;
    };
    DropdownComponentSelectorsFactory.getSelectedOptionXPath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getControlXPath(label, isContains, insidePopup) + "//*[contains(@class,'" + this.selector + "__input-text')]";
    };
    DropdownComponentSelectorsFactory.getOptionsXPath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getControlXPath(label, isContains, insidePopup) + "//*[contains(@class,'" + this.selector + "__options')]//df-option";
    };
    DropdownComponentSelectorsFactory.getOptionXPath = function (label, optionLabel, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getControlXPath(label, false, insidePopup) + "//*[contains(@class,'" + this.selector + "__options')]\n        //df-option[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForText(optionLabel, isContains) + "]";
    };
    DropdownComponentSelectorsFactory.selector = "df-select";
    return DropdownComponentSelectorsFactory;
}());
exports.DropdownComponentSelectorsFactory = DropdownComponentSelectorsFactory;
//# sourceMappingURL=dropdown-component-selectors-factory.js.map