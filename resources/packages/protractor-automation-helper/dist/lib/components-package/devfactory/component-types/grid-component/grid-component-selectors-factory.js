"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_helpers_factory_1 = require("../../component-helpers/component-helpers-factory");
var checkbox_component_selectors_factory_1 = require("../checkbox-component/checkbox-component-selectors-factory");
var GridComponentSelectorsFactory = /** @class */ (function () {
    function GridComponentSelectorsFactory() {
    }
    GridComponentSelectorsFactory.getCellByText = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this.selector + "//div[contains(@class,\"ngCellText\")]\n            //span[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForText(text, isContains) + "]";
    };
    GridComponentSelectorsFactory.getColumnItemXPath = function (columnName, listName, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this.selector + "//td[@data-header=\"" + columnName + "\"]\n        //div[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForDot(listName, isContains) + "]";
    };
    GridComponentSelectorsFactory.getSelectItemCheckBoxXPath = function (dataHeaderName, dataHeaderItemName) {
        return "//" + this
            .selector + "//td[@data-header=\"" + dataHeaderName + "\"]/div[contains(normalize-space(.),\"" + dataHeaderItemName + "\")]\n        /parent::td/preceding-sibling::td//i[contains(@class,\"fa-square\")]";
    };
    GridComponentSelectorsFactory.columnItemButtonXPath = function (columnName, listName, buttonName, isContainsInButtonName) {
        if (isContainsInButtonName === void 0) { isContainsInButtonName = false; }
        return "//" + this
            .selector + "//td[@data-header=\"" + columnName + "\"]//div[contains(normalize-space(.),\"" + listName + "\")]\n        //following-sibling::div//a[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForDot(buttonName, isContainsInButtonName) + "]";
    };
    GridComponentSelectorsFactory.getDropDownOptionsXPath = function (dropDownName) {
        return "//" + this.selector + "//p[normalize-space(text())=\"" + dropDownName + "\"]\n        /following-sibling::ul//span[@class=\"" + checkbox_component_selectors_factory_1.CheckboxComponentSelectorsFactory.selector + "__label\"]";
    };
    GridComponentSelectorsFactory.getDropDownOptionXPath = function (dropDownName, optionName) {
        return "//" + this.getDropDownOptionsXPath(dropDownName) + "[normalize-space(.)=\"" + optionName + "\"]";
    };
    GridComponentSelectorsFactory.selector = "table[@df-table]";
    return GridComponentSelectorsFactory;
}());
exports.GridComponentSelectorsFactory = GridComponentSelectorsFactory;
//# sourceMappingURL=grid-component-selectors-factory.js.map