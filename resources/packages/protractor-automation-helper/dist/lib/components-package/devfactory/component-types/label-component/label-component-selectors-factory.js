"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LabelComponentSelectorsFactory = /** @class */ (function () {
    function LabelComponentSelectorsFactory() {
    }
    LabelComponentSelectorsFactory.getInfoIconXPath = function (labelName) {
        return "//" + this
            .selector + "[normalize-space(.)=\"" + labelName + "\"]/following-sibling::i";
    };
    LabelComponentSelectorsFactory.selector = "label";
    return LabelComponentSelectorsFactory;
}());
exports.LabelComponentSelectorsFactory = LabelComponentSelectorsFactory;
//# sourceMappingURL=label-component-selectors-factory.js.map