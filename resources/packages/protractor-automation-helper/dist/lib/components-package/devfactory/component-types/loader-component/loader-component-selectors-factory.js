"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoaderComponentSelectorsFactory = /** @class */ (function () {
    function LoaderComponentSelectorsFactory() {
    }
    LoaderComponentSelectorsFactory.getControlXPath = function () {
        return "//" + this.selector;
    };
    LoaderComponentSelectorsFactory.selector = "df-loading-spinner";
    return LoaderComponentSelectorsFactory;
}());
exports.LoaderComponentSelectorsFactory = LoaderComponentSelectorsFactory;
//# sourceMappingURL=loader-component-selectors-factory.js.map