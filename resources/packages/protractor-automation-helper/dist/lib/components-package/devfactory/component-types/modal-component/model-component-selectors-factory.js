"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ModelComponentSelectorsFactory = /** @class */ (function () {
    function ModelComponentSelectorsFactory() {
    }
    Object.defineProperty(ModelComponentSelectorsFactory, "modelPopupBodyXpath", {
        get: function () {
            return "//" + this.selector + "//div[contains(@class,'popover-body')]";
        },
        enumerable: true,
        configurable: true
    });
    ModelComponentSelectorsFactory.getModelPopupXpath = function (insidePopup) {
        return insidePopup ? "//" + this.selector : "";
    };
    ModelComponentSelectorsFactory.selector = "ngb-modal-window";
    return ModelComponentSelectorsFactory;
}());
exports.ModelComponentSelectorsFactory = ModelComponentSelectorsFactory;
//# sourceMappingURL=model-component-selectors-factory.js.map