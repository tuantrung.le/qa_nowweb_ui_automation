"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_helpers_factory_1 = require("../../component-helpers/component-helpers-factory");
var RadioComponentSelectorsFactory = /** @class */ (function () {
    function RadioComponentSelectorsFactory() {
    }
    RadioComponentSelectorsFactory.getDfOptionsByTextXPath = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this.selector + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForDot(text, isContains);
    };
    RadioComponentSelectorsFactory.selector = "df-option";
    return RadioComponentSelectorsFactory;
}());
exports.RadioComponentSelectorsFactory = RadioComponentSelectorsFactory;
//# sourceMappingURL=radio-component-selectors-factory.js.map