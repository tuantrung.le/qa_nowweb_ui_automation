"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SectionComponentSelectorsFactory = /** @class */ (function () {
    function SectionComponentSelectorsFactory() {
    }
    SectionComponentSelectorsFactory.getControlXPath = function (title) {
        return "//h2[contains(@class,\"comp-section__header\") and normalize-space(.)=\"" + title + "\"]";
    };
    SectionComponentSelectorsFactory.getIconXPath = function (title, iconClass) {
        return this.getControlXPath(title) + "//following-sibling::div//button[i[contains(@class,\"" + iconClass + "\")]]";
    };
    return SectionComponentSelectorsFactory;
}());
exports.SectionComponentSelectorsFactory = SectionComponentSelectorsFactory;
//# sourceMappingURL=section-component-selectors-factory.js.map