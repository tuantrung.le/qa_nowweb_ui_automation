"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SidebarMenuComponentSelectorsFactory = /** @class */ (function () {
    function SidebarMenuComponentSelectorsFactory() {
    }
    SidebarMenuComponentSelectorsFactory.sideBarSubMenuItemXPath = function (text) {
        return this
            .selector + "//div[contains(@class,\"df-sidebar\")]//a[contains(normalize-space(.),\"" + text + "\")]";
    };
    SidebarMenuComponentSelectorsFactory.selector = "df-sidebar";
    return SidebarMenuComponentSelectorsFactory;
}());
exports.SidebarMenuComponentSelectorsFactory = SidebarMenuComponentSelectorsFactory;
//# sourceMappingURL=sidebar-menu-component-selectors-factory.js.map