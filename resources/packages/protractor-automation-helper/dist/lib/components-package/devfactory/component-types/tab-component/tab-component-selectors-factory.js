"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var section_component_selectors_factory_1 = require("../section-component/section-component-selectors-factory");
var TabComponentSelectorsFactory = /** @class */ (function () {
    function TabComponentSelectorsFactory() {
    }
    TabComponentSelectorsFactory.activeTabWithTextXPath = function (sectionName, tab) {
        return (section_component_selectors_factory_1.SectionComponentSelectorsFactory.getControlXPath(sectionName) + "\n        //*[contains(@class, \"link--active\")][normalize-space(.)=\"" +
            tab +
            "\"]");
    };
    return TabComponentSelectorsFactory;
}());
exports.TabComponentSelectorsFactory = TabComponentSelectorsFactory;
//# sourceMappingURL=tab-component-selectors-factory.js.map