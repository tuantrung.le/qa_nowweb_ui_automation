"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var model_component_selectors_factory_1 = require("../modal-component/model-component-selectors-factory");
var TextBoxComponentSelectorsFactory = /** @class */ (function () {
    function TextBoxComponentSelectorsFactory() {
    }
    TextBoxComponentSelectorsFactory.getTextBoxByPlaceholderXpath = function (placeholder, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this
            .selector + "//input[normalize-space(@placeholder)=\"" + placeholder + "\"]";
    };
    TextBoxComponentSelectorsFactory.getErrorMessageInPopUp = function (formControlName) {
        return "//input[@formcontrolname=\"" + formControlName + "\"]/following-sibling::div[contains(@class,\"form__error\")]";
    };
    TextBoxComponentSelectorsFactory.getTextBoxByLabelXpath = function (label, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getXpathForForTextboxFollowingSiblingByLabel(label, insidePopup) + "input";
    };
    TextBoxComponentSelectorsFactory.getTextBoxErrorLabelXpath = function (label, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getXpathForForTextboxFollowingSiblingByLabel(label, insidePopup) + "div[contains(@class,'form__error')]";
    };
    TextBoxComponentSelectorsFactory.getXpathForForTextboxFollowingSiblingByLabel = function (label, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this
            .selector + "//label[normalize-space(.)='" + label + "']//following-sibling::";
    };
    TextBoxComponentSelectorsFactory.getTextBoxByFormControlNameXpath = function (formControlName, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return model_component_selectors_factory_1.ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //*[@formcontrolname='" + formControlName + "']";
    };
    TextBoxComponentSelectorsFactory.selector = "df-input-container";
    return TextBoxComponentSelectorsFactory;
}());
exports.TextBoxComponentSelectorsFactory = TextBoxComponentSelectorsFactory;
//# sourceMappingURL=text-component-selectors-factory.js.map