"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ToastComponentSelectorsFactory = /** @class */ (function () {
    function ToastComponentSelectorsFactory() {
    }
    Object.defineProperty(ToastComponentSelectorsFactory, "toastHeaderXPath", {
        get: function () {
            return this.toastBodyXPath + " > h1";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ToastComponentSelectorsFactory, "toastBodyXPath", {
        get: function () {
            return this.selector + " .df-toast__body";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ToastComponentSelectorsFactory, "toastContentXPath", {
        get: function () {
            return this.toastBodyXPath + " > p";
        },
        enumerable: true,
        configurable: true
    });
    ToastComponentSelectorsFactory.selector = "df-toast";
    return ToastComponentSelectorsFactory;
}());
exports.ToastComponentSelectorsFactory = ToastComponentSelectorsFactory;
//# sourceMappingURL=toast-component-selectors-factory.js.map