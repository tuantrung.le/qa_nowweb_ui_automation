"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TooltipComponentSelectorsFactory = /** @class */ (function () {
    function TooltipComponentSelectorsFactory() {
    }
    TooltipComponentSelectorsFactory.getTooltipXPath = function (message) {
        return "//" + this
            .selector + "//div[contains(@class,'tooltip-inner') and contains(normalize-space(.),'" + message + "')]";
    };
    TooltipComponentSelectorsFactory.selector = "ngb-modal-window";
    return TooltipComponentSelectorsFactory;
}());
exports.TooltipComponentSelectorsFactory = TooltipComponentSelectorsFactory;
//# sourceMappingURL=tooltip-component-selectors-factory.js.map