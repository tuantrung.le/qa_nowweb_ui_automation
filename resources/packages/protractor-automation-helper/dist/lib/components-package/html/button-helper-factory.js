"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_helpers_factory_1 = require("../devfactory/component-helpers/component-helpers-factory");
var ButtonHelperFactory = /** @class */ (function () {
    function ButtonHelperFactory() {
    }
    ButtonHelperFactory.getButtonByExactTextXPath = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//button[" + component_helpers_factory_1.ComponentHelpersFactory.getXPathFunctionForDot(text, isContains) + "]";
    };
    return ButtonHelperFactory;
}());
exports.ButtonHelperFactory = ButtonHelperFactory;
//# sourceMappingURL=button-helper-factory.js.map