"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var wait_helper_factory_1 = require("./wait-helper-factory");
/**
 * Page helper for general utility
 */
var PageHelperFactory = /** @class */ (function () {
    function PageHelperFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
        this.waitHelperFactory = new wait_helper_factory_1.WaitHelperFactory(this._browser, this._by);
    }
    // Known issue for chrome, direct maximize window doesn't work
    /**
     * To maximize the this._browser window
     */
    PageHelperFactory.prototype.maximizeWindow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var Size, windowSize, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        Size = /** @class */ (function () {
                            function Size() {
                                this.width = 0;
                                this.height = 0;
                            }
                            return Size;
                        }());
                        return [4 /*yield*/, this.executeScript(function () {
                                return {
                                    width: window.screen.availWidth,
                                    height: window.screen.availHeight
                                };
                            })];
                    case 1:
                        windowSize = _a.sent();
                        result = windowSize;
                        return [2 /*return*/, this.setWindowSize(result.width, result.height)];
                }
            });
        });
    };
    /**
     * Sets window size
     * @param {number} width
     * @param {number} height
     */
    PageHelperFactory.prototype.setWindowSize = function (width, height) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._browser.driver
                        .manage()
                        .window()
                        .setSize(width, height)];
            });
        });
    };
    /**
     * Wrapper for executing javascript code
     * @param {string | Function} script
     * @param varAargs
     * @returns {promise.Promise<any>}
     */
    PageHelperFactory.prototype.executeScript = function (script) {
        var varAargs = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            varAargs[_i - 1] = arguments[_i];
        }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._browser.driver.executeScript(script, varAargs)];
            });
        });
    };
    /**
     * Wrapper to return an active element
     * @returns {any}

     public  async getFocusedElement() {
    return this._browser.driver.switchTo().activeElement()
  }*/
    /**
     * Switch to a new tab if this._browser has availability
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    PageHelperFactory.prototype.switchToNewTabIfAvailable = function () {
        return __awaiter(this, void 0, void 0, function () {
            var handles, newWindowHandle, url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._browser.getAllWindowHandles()];
                    case 1:
                        handles = _a.sent();
                        newWindowHandle = handles[1];
                        if (!newWindowHandle) return [3 /*break*/, 3];
                        return [4 /*yield*/, this._browser.switchTo().window(newWindowHandle)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [4 /*yield*/, this._browser.getCurrentUrl()];
                    case 4:
                        url = _a.sent();
                        // Avoiding bootstraping issue, Known issue
                        // Error: Error while waiting for Protractor to sync with the page:
                        // "window.angular is undefined. This could be either because this is a non-angular page or
                        // because your test involves client-side navigation, which can interfere with Protractor's bootstrapping.
                        // See http://git.io/v4gXM for details
                        return [2 /*return*/, this._browser.driver.get(url)];
                }
            });
        });
    };
    /**
     * Gets html attribute value
     * @param {WebElementPromise} elem
     * @param {string} attribute
     * @returns {string} attribute value
     */
    PageHelperFactory.prototype.getAttributeValue = function (elem, attribute) {
        return __awaiter(this, void 0, void 0, function () {
            var attributeValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, elem.getAttribute(attribute)];
                    case 1:
                        attributeValue = _a.sent();
                        return [2 /*return*/, attributeValue.trim()];
                }
            });
        });
    };
    /**
     * Click on element
     * @param {any} targetElement
     * @returns {any}
     */
    PageHelperFactory.prototype.click = function (targetElement) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitHelperFactory.waitForElementToBeClickable(targetElement)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, targetElement.click()];
                }
            });
        });
    };
    /**
     * Click on the element and wait for it to get hidden
     * @param {any} targetElement
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    PageHelperFactory.prototype.clickAndWaitForElementToHide = function (targetElement) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitHelperFactory.waitForElementToBeClickable(targetElement)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, targetElement.click()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, this.waitHelperFactory.waitForElementToBeHidden(targetElement)];
                }
            });
        });
    };
    /**
     * Gets promise for current url
     * @returns {any}
     */
    PageHelperFactory.prototype.currentUrl = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._browser.getCurrentUrl()];
            });
        });
    };
    return PageHelperFactory;
}());
exports.PageHelperFactory = PageHelperFactory;
//# sourceMappingURL=page-helper-factory.js.map