"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var wait_helper_factory_1 = require("./wait-helper-factory");
var page_helper_factory_1 = require("./page-helper-factory");
var html_helper_factory_1 = require("../misc-utils/html-helper-factory");
var protractor_1 = require("protractor");
var TextBoxHelperFactory = /** @class */ (function () {
    function TextBoxHelperFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
        this.waitHelperFactory = new wait_helper_factory_1.WaitHelperFactory(this._browser, this._by);
        this.pageHelperFactory = new page_helper_factory_1.PageHelperFactory(this._browser, this._by);
    }
    /**
     * Clears the existing text from an input elements
     * @param {any} locator
     */
    TextBoxHelperFactory.prototype.clearText = function (locator) {
        return __awaiter(this, void 0, void 0, function () {
            var ctrl, command;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ctrl = protractor_1.protractor.Key.CONTROL;
                        if (this._browser.platform.indexOf("Mac")) {
                            ctrl = protractor_1.protractor.Key.COMMAND;
                        }
                        command = protractor_1.protractor.Key.chord(ctrl, "a") + protractor_1.protractor.Key.BACK_SPACE;
                        return [4 /*yield*/, locator.sendKeys(command)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, locator.clear()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Send Keys to an input elements once it becomes available
     * @param {any} locator for element
     * @param {string} value to be sent
     * @param {boolean} sendEnter for sending an enter key
     */
    TextBoxHelperFactory.prototype.sendKeys = function (locator, value, sendEnter) {
        if (sendEnter === void 0) { sendEnter = false; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitHelperFactory.waitForElement(locator)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.clearText(locator)];
                    case 2:
                        _a.sent();
                        // On IE, text is sometimes not well sent, this is a workaround
                        return [4 /*yield*/, locator.sendKeys(value)];
                    case 3:
                        // On IE, text is sometimes not well sent, this is a workaround
                        _a.sent();
                        if (!sendEnter) return [3 /*break*/, 5];
                        return [4 /*yield*/, locator.sendKeys(protractor_1.protractor.Key.ENTER)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Checks whether an input box has particular value or not
     * @param {any} locator
     * @param {string} text
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    TextBoxHelperFactory.prototype.hasValue = function (locator, text) {
        return __awaiter(this, void 0, void 0, function () {
            var val;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.pageHelperFactory.getAttributeValue(locator, html_helper_factory_1.HtmlHelperFactory.attributes.value)];
                    case 1:
                        val = _a.sent();
                        return [2 /*return*/, val === text];
                }
            });
        });
    };
    return TextBoxHelperFactory;
}());
exports.TextBoxHelperFactory = TextBoxHelperFactory;
//# sourceMappingURL=textbox-helper-factory.js.map