"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var constants_factory_1 = require("../misc-utils/constants-factory");
var WaitHelperFactory = /** @class */ (function () {
    function WaitHelperFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
        this.EC = this._browser.ExpectedConditions;
        this.timeoutMessage = " not found in time";
        this.DEFAULT_TIMEOUT = constants_factory_1.ConstantsFactory.timeout.xxl;
    }
    /**
     * Default timeout for promises
     * @type {number}
     */
    /**
     * Wait for an element to exist
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    WaitHelperFactory.prototype.waitForElement = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.presenceOf, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Wait for an element to display
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    WaitHelperFactory.prototype.waitForElementToBeDisplayed = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.visibilityOf, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Wait for an element to hide
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     * @returns {any}
     */
    WaitHelperFactory.prototype.waitForElementToBeHidden = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.invisibilityOf, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Wait for an element to become clickable
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    WaitHelperFactory.prototype.waitForElementToBeClickable = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.elementToBeClickable, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Common waiter
     * @param {Function} functionName
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     * @returns {Promise<void>}
     */
    WaitHelperFactory.prototype.waiter = function (functionName, targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            var waiterResponsible;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        waiterResponsible = functionName(targetElement);
                        if (message.length === 0) {
                            message = targetElement.locator().toString() + this.timeoutMessage;
                        }
                        return [4 /*yield*/, this._browser.wait(waiterResponsible, timeout, message)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return WaitHelperFactory;
}());
exports.WaitHelperFactory = WaitHelperFactory;
//# sourceMappingURL=wait-helper-factory.js.map