"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CommonLabelFactory = /** @class */ (function () {
    function CommonLabelFactory() {
    }
    CommonLabelFactory.save = "Save";
    CommonLabelFactory.ok = "Ok";
    CommonLabelFactory.saveAs = "Save As";
    CommonLabelFactory.saveAsWithDots = "Save as...";
    CommonLabelFactory.done = "Done";
    CommonLabelFactory.cancel = "Cancel";
    CommonLabelFactory.add = "Add";
    CommonLabelFactory.edit = "Edit";
    CommonLabelFactory.yes = "Yes";
    CommonLabelFactory.no = "No";
    return CommonLabelFactory;
}());
exports.CommonLabelFactory = CommonLabelFactory;
//# sourceMappingURL=common-label-factory.js.map