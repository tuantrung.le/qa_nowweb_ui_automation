"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * General purpose constants
 */
var ConstantsFactory = /** @class */ (function () {
    function ConstantsFactory() {
    }
    ConstantsFactory.stringEmpty = "";
    /**
     * Timeout collection to meet various needs
     * @type {{xs: number; s: number; m: number; l: number; xl: number; xxl: number; xxxl: number}}
     */
    ConstantsFactory.timeout = {
        xs: 2000,
        s: 5000,
        m: 10000,
        l: 25000,
        xl: 50000,
        xxl: 75000,
        xxxl: 200000
    };
    return ConstantsFactory;
}());
exports.ConstantsFactory = ConstantsFactory;
//# sourceMappingURL=constants-factory.js.map