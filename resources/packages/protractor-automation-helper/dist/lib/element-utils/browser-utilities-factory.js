"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BrowserUtilitiesFactory = /** @class */ (function () {
    function BrowserUtilitiesFactory(_capabilities) {
        this._capabilities = _capabilities;
    }
    BrowserUtilitiesFactory.prototype.isIE10 = function () {
        return (this._capabilities.get("browserName") === "internet explorer" &&
            this._capabilities.get("version") === "10");
    };
    BrowserUtilitiesFactory.prototype.isFirefox = function () {
        return this._capabilities.get("browserName") === "firefox";
    };
    BrowserUtilitiesFactory.prototype.isEdge = function () {
        return this._capabilities.get("browserName") === "MicrosoftEdge";
    };
    BrowserUtilitiesFactory.prototype.isSafari = function () {
        return this._capabilities.get("browserName") === "safari";
    };
    return BrowserUtilitiesFactory;
}());
exports.BrowserUtilitiesFactory = BrowserUtilitiesFactory;
//# sourceMappingURL=browser-utilities-factory.js.map