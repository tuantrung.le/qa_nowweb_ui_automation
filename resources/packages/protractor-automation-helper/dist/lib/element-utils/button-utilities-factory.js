"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ButtonUtilitiesFactory = /** @class */ (function () {
    function ButtonUtilitiesFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
    }
    ButtonUtilitiesFactory.prototype.getButtonById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//button[@id='" + elementId + "']"));
    };
    return ButtonUtilitiesFactory;
}());
exports.ButtonUtilitiesFactory = ButtonUtilitiesFactory;
//# sourceMappingURL=button-utilities-factory.js.map