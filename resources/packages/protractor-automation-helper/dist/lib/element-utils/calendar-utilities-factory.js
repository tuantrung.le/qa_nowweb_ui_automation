"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var browser_utilities_factory_1 = require("./browser-utilities-factory");
var CalendarUtilitiesFactory = /** @class */ (function () {
    function CalendarUtilitiesFactory(_browser, _by, _protractor) {
        this._browser = _browser;
        this._by = _by;
        this._protractor = _protractor;
        this.DF_DATEPICKER_ARROW = "df-datepicker__ctrl-btn df-datepicker__btn df-datepicker__ctrl-btn--";
    }
    CalendarUtilitiesFactory.prototype.getDateById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//df-datepicker[@id='" + elementId + "']"));
    };
    CalendarUtilitiesFactory.prototype.getTimeById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//df-timepicker[@id='" + elementId + "']"));
    };
    CalendarUtilitiesFactory.prototype.setDateById = function (elementId, value, direction) {
        var _this = this;
        if (direction === void 0) { direction = "previous"; }
        var currentYear = new Date().getFullYear();
        // tslint:disable-next-line:radix
        var years = currentYear - parseInt(value.substring(6, 11));
        var day = value.substring(0, 2);
        var datePicker = this._browser.driver.findElement(this._by.xpath("//df-datepicker[@id='" + elementId + "']//input"));
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new browser_utilities_factory_1.BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10() || browserUtilities.isSafari()) {
                datePicker.click();
                var directionBtn = _this._browser.driver.findElement(_this._by.xpath("//df-datepicker[@id='" + elementId + "']//button[@class='" +
                    _this.DF_DATEPICKER_ARROW +
                    (direction + "']")));
                for (var i = 0; i < years * 12; i++) {
                    directionBtn.click();
                }
                var dayElement = _this._browser.driver.findElement(_this._by.xpath("//df-datepicker[@id='" +
                    elementId +
                    "']//span[contains(text(), '" +
                    day +
                    "')]"));
                dayElement.click();
            }
            else {
                datePicker.clear();
                datePicker.sendKeys(value);
                datePicker.sendKeys(_this._protractor.Key.ENTER);
            }
        });
    };
    return CalendarUtilitiesFactory;
}());
exports.CalendarUtilitiesFactory = CalendarUtilitiesFactory;
//# sourceMappingURL=calendar-utilities-factory.js.map