"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var browser_utilities_factory_1 = require("./browser-utilities-factory");
var DropdownUtilitiesFactory = /** @class */ (function () {
    function DropdownUtilitiesFactory(_browser, _by, _expectedConditions) {
        this._browser = _browser;
        this._by = _by;
        this._expectedConditions = _expectedConditions;
        this.DF_SELECT_OPTIONS_LIST = "df-select__options-list";
    }
    DropdownUtilitiesFactory.prototype.openDropdownByCss = function (selector) {
        this._browser.driver.executeScript("document.querySelector('" +
            selector +
            "').setAttribute('style', 'opacity: 1;');");
    };
    DropdownUtilitiesFactory.prototype.openDropdownById = function (id) {
        this._browser.driver.executeScript("document.querySelector('#" +
            id +
            " .df-select__options').setAttribute('style', 'opacity: 1;');");
    };
    DropdownUtilitiesFactory.prototype.closeDropdownByCss = function (selector) {
        this._browser.driver.executeScript("document.querySelector('" +
            selector +
            "').setAttribute('style', 'opacity: 0;');");
    };
    DropdownUtilitiesFactory.prototype.closeDropdownById = function (id) {
        this._browser.driver.executeScript("document.querySelector('#" +
            id +
            " .df-select__options').setAttribute('style', 'opacity: 0;');");
    };
    DropdownUtilitiesFactory.prototype.getDropdownById = function (elementId) {
        var path = this._by.xpath("//df-select[@id='" + elementId + "']");
        return this._browser.driver.findElement(path);
    };
    DropdownUtilitiesFactory.prototype.setDropdownOptionIndexById = function (elementId, optionIndex) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new browser_utilities_factory_1.BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[" + optionIndex + "]");
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
        });
    };
    DropdownUtilitiesFactory.prototype.setDropdownValueById = function (elementId, value) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new browser_utilities_factory_1.BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" +
                elementId +
                "']//df-option[@ng-reflect-value='" +
                value +
                "']");
            if (browserUtilities.isEdge() || browserUtilities.isFirefox()) {
                var optionsListPath = _this._by.xpath("//df-select[@id='" +
                    elementId +
                    "']//div[@class='" +
                    _this.DF_SELECT_OPTIONS_LIST +
                    "']");
                _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(optionsListPath)));
                var firstOptionPath = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[1]");
                _this._browser.wait(_this._expectedConditions.presenceOf(_this._browser.element(firstOptionPath)));
                _this.scrollIntoOption(elementId, value);
            }
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
        });
    };
    DropdownUtilitiesFactory.prototype.setMultiDropdownOptionIndexById = function (elementId, optionIndex) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new browser_utilities_factory_1.BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[" + optionIndex + "]");
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
            else {
                _this._browser.driver
                    .findElement(_this._by.xpath("//df-select[@id='" + elementId + "']"))
                    .click();
            }
        });
    };
    DropdownUtilitiesFactory.prototype.setDropdownValueByIdWithoutSync = function (elementId, value) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new browser_utilities_factory_1.BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" +
                elementId +
                "']//df-option[@ng-reflect-value='" +
                value +
                "']");
            if (browserUtilities.isEdge() || browserUtilities.isFirefox()) {
                var optionsListPath = _this._by.xpath("//df-select[@id='" +
                    elementId +
                    "']//div[@class='" +
                    _this.DF_SELECT_OPTIONS_LIST +
                    "']");
                _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(optionsListPath)));
                var firstOptionPath = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[1]");
                _this._browser.wait(_this._expectedConditions.presenceOf(_this._browser.element(firstOptionPath)));
                _this.scrollIntoOption(elementId, value);
            }
            _this._browser.waitForAngularEnabled(false);
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
        });
    };
    DropdownUtilitiesFactory.prototype.scrollIntoOption = function (elementId, optionValue) {
        this._browser.driver.executeScript("document.querySelector(\"#" +
            elementId +
            " ." +
            this.DF_SELECT_OPTIONS_LIST +
            " [ng-reflect-value='" +
            optionValue +
            "']\").scrollIntoView()");
    };
    return DropdownUtilitiesFactory;
}());
exports.DropdownUtilitiesFactory = DropdownUtilitiesFactory;
//# sourceMappingURL=dropdown-utilities-factory.js.map