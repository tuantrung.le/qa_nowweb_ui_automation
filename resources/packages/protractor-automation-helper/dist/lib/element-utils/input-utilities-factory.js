"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InputUtilitiesFactory = /** @class */ (function () {
    function InputUtilitiesFactory(_browser, _by, _protractor) {
        this._browser = _browser;
        this._by = _by;
        this._protractor = _protractor;
    }
    InputUtilitiesFactory.prototype.getInputById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//input[@id='" + elementId + "']"));
    };
    InputUtilitiesFactory.prototype.setInputById = function (elementId, value) {
        var field = this._browser.driver.findElement(this._by.xpath("//input[@id='" + elementId + "']"));
        field.sendKeys(value);
        field.sendKeys(this._protractor.Key.ENTER);
    };
    return InputUtilitiesFactory;
}());
exports.InputUtilitiesFactory = InputUtilitiesFactory;
//# sourceMappingURL=input-utilities-factory.js.map