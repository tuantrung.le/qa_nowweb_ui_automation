"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SectionUtilitiesFactory = /** @class */ (function () {
    function SectionUtilitiesFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
    }
    SectionUtilitiesFactory.prototype.getSectionById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//section[@id='" + elementId + "']"));
    };
    return SectionUtilitiesFactory;
}());
exports.SectionUtilitiesFactory = SectionUtilitiesFactory;
//# sourceMappingURL=section-utilities-factory.js.map