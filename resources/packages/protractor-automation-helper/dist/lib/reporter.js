"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Reporter = /** @class */ (function () {
    function Reporter() {
    }
    /**
     * Generates csv and jtl files based on provided browser logs
     * @param {any} browserLogs
     * @param {any} resolve
     */
    Reporter.performanceOnComplete = function (browserInstance, resolve) {
        return __awaiter(this, void 0, void 0, function () {
            var delimiter, endOfLine, requestsString, timeString, requestsSent, responseReceived, browserLogs, _i, _a, key, browserLog, message, totalTimes, requestNumbers, xhrData, jtlData, jtlEndpointData, _b, _c, id, requestWalltime, currentType, currentPage, currentUrl, elapsedTime, responseCode, responseStatus, isSuccess, endpoint, fractionDigits, csvData, totalRequests, totalTime, totalAverage, _d, _e, page, _f, _g, type, typeRequestNumber, typeTime, _h, _j, endpoint;
            return __generator(this, function (_k) {
                switch (_k.label) {
                    case 0:
                        _k.trys.push([0, , 3, 4]);
                        if (!process.env.BUILD_NUMBER) return [3 /*break*/, 2];
                        delimiter = ",";
                        endOfLine = "\r\n";
                        requestsString = "requests";
                        timeString = "time";
                        requestsSent = {};
                        responseReceived = {};
                        return [4 /*yield*/, browserInstance.manage().logs().get("performance")];
                    case 1:
                        browserLogs = _k.sent();
                        for (_i = 0, _a = Object.keys(browserLogs); _i < _a.length; _i++) {
                            key = _a[_i];
                            browserLog = browserLogs[key];
                            message = JSON.parse(browserLog.message).message;
                            if (message.method === "Network.requestWillBeSent") {
                                requestsSent[message.params.requestId] = message;
                            }
                            else if (message.method === "Network.responseReceived") {
                                responseReceived[message.params.requestId] = message;
                            }
                        }
                        totalTimes = [];
                        requestNumbers = [];
                        xhrData = [];
                        jtlData = "timeStamp,elapsed,label,responseCode,responseMessage,success,failureMessage,bytes,sentBytes,allThreads,URL";
                        jtlEndpointData = jtlData;
                        for (_b = 0, _c = Object.keys(responseReceived); _b < _c.length; _b++) {
                            id = _c[_b];
                            if (requestsSent[id]) {
                                requestWalltime = requestsSent[id].params.wallTime.toString().split(".").join("");
                                currentType = responseReceived[id].params.type;
                                currentPage = this.quoteString(requestsSent[id].params.documentURL);
                                currentUrl = requestsSent[id].params.request.url;
                                elapsedTime = Math.round((responseReceived[id].params.timestamp - requestsSent[id].params.timestamp) * 1000);
                                responseCode = responseReceived[id].params.response.status;
                                responseStatus = this.quoteString(responseReceived[id].params.response.statusText);
                                isSuccess = false;
                                if (responseCode >= 200 && responseCode <= 209) {
                                    isSuccess = true;
                                }
                                if (!totalTimes[currentPage]) {
                                    totalTimes[currentPage] = [];
                                    requestNumbers[currentPage] = [];
                                }
                                if (totalTimes[currentPage][currentType]) {
                                    totalTimes[currentPage][currentType] += elapsedTime;
                                    requestNumbers[currentPage][currentType]++;
                                }
                                else {
                                    totalTimes[currentPage][currentType] = elapsedTime;
                                    requestNumbers[currentPage][currentType] = 1;
                                }
                                if (currentType === "XHR") {
                                    endpoint = this.quoteString(currentUrl.split("?")[0]);
                                    if (!xhrData[endpoint]) {
                                        xhrData[endpoint] = {};
                                        xhrData[endpoint][requestsString] = 0;
                                        xhrData[endpoint][timeString] = 0;
                                    }
                                    xhrData[endpoint][requestsString]++;
                                    xhrData[endpoint][timeString] += elapsedTime;
                                    jtlEndpointData += endOfLine;
                                    jtlEndpointData += [
                                        requestWalltime,
                                        elapsedTime,
                                        endpoint,
                                        responseCode,
                                        responseStatus,
                                        isSuccess,
                                        responseStatus,
                                        0,
                                        0,
                                        0,
                                        currentPage
                                    ].join(delimiter);
                                }
                                if (currentUrl.includes("data:image")) {
                                    currentUrl = currentUrl.split(";")[0];
                                }
                                jtlData += endOfLine;
                                jtlData += [
                                    requestWalltime,
                                    elapsedTime,
                                    currentPage,
                                    responseCode,
                                    responseStatus,
                                    isSuccess,
                                    responseStatus,
                                    0,
                                    0,
                                    0,
                                    this.quoteString(currentUrl)
                                ].join(delimiter);
                            }
                        }
                        fractionDigits = 3;
                        csvData = ",Requests,Total time,Average time";
                        totalRequests = 0;
                        totalTime = 0;
                        totalAverage = 0;
                        for (_d = 0, _e = Object.keys(totalTimes); _d < _e.length; _d++) {
                            page = _e[_d];
                            console.log(page);
                            csvData += endOfLine + page;
                            for (_f = 0, _g = Object.keys(totalTimes[page]); _f < _g.length; _f++) {
                                type = _g[_f];
                                typeRequestNumber = requestNumbers[page][type];
                                typeTime = totalTimes[page][type];
                                totalRequests += typeRequestNumber;
                                totalTime += typeTime;
                                totalAverage += typeTime / typeRequestNumber;
                                console.log(type + "\n            requests: " + typeRequestNumber + "\n            total time: " + typeTime.toFixed(fractionDigits) + "\n            average time: " + (typeTime / typeRequestNumber).toFixed(fractionDigits));
                                csvData += endOfLine;
                                csvData += [
                                    type,
                                    typeRequestNumber,
                                    typeTime.toFixed(fractionDigits),
                                    (typeTime / typeRequestNumber).toFixed(fractionDigits)
                                ].join(delimiter);
                            }
                        }
                        console.log("Total\n            requests: " + totalRequests + "\n            total time: " + totalTime.toFixed(fractionDigits) + "\n            average time: " + totalAverage.toFixed(fractionDigits));
                        csvData += endOfLine;
                        csvData += [
                            "Total",
                            totalRequests,
                            totalTime.toFixed(fractionDigits),
                            totalAverage.toFixed(fractionDigits)
                        ].join(delimiter);
                        csvData += endOfLine;
                        for (_h = 0, _j = Object.keys(xhrData); _h < _j.length; _h++) {
                            endpoint = _j[_h];
                            csvData += endOfLine;
                            csvData += [
                                endpoint,
                                xhrData[endpoint][requestsString],
                                xhrData[endpoint][timeString].toFixed(fractionDigits),
                                (xhrData[endpoint][timeString] / xhrData[endpoint][requestsString]).toFixed(fractionDigits)
                            ].join(delimiter);
                        }
                        this.saveFile("performance.csv", csvData);
                        this.saveFile("pages.jtl", jtlData);
                        this.saveFile("endpoints.jtl", jtlEndpointData);
                        _k.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        resolve();
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Reporter.saveFile = function (filePath, fileData) {
        var fs = require("fs");
        fs.writeFile(filePath, fileData, function (error) {
            if (error) {
                console.error(error);
                return error;
            }
        });
    };
    Reporter.quoteString = function (csvString) {
        return "\"" + csvString + "\"";
    };
    return Reporter;
}());
exports.Reporter = Reporter;
//# sourceMappingURL=reporter.js.map