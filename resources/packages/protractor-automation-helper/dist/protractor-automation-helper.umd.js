(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.protractorAutomationHelper = {})));
}(this, (function (exports) { 'use strict';

var ButtonUtilitiesFactory = /** @class */ (function () {
    function ButtonUtilitiesFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
    }
    ButtonUtilitiesFactory.prototype.getButtonById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//button[@id='" + elementId + "']"));
    };
    return ButtonUtilitiesFactory;
}());

var InputUtilitiesFactory = /** @class */ (function () {
    function InputUtilitiesFactory(_browser, _by, _protractor) {
        this._browser = _browser;
        this._by = _by;
        this._protractor = _protractor;
    }
    InputUtilitiesFactory.prototype.getInputById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//input[@id='" + elementId + "']"));
    };
    InputUtilitiesFactory.prototype.setInputById = function (elementId, value) {
        var field = this._browser.driver.findElement(this._by.xpath("//input[@id='" + elementId + "']"));
        field.sendKeys(value);
        field.sendKeys(this._protractor.Key.ENTER);
    };
    return InputUtilitiesFactory;
}());

var SectionUtilitiesFactory = /** @class */ (function () {
    function SectionUtilitiesFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
    }
    SectionUtilitiesFactory.prototype.getSectionById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//section[@id='" + elementId + "']"));
    };
    return SectionUtilitiesFactory;
}());

var BrowserUtilitiesFactory = /** @class */ (function () {
    function BrowserUtilitiesFactory(_capabilities) {
        this._capabilities = _capabilities;
    }
    BrowserUtilitiesFactory.prototype.isIE10 = function () {
        return (this._capabilities.get("browserName") === "internet explorer" &&
            this._capabilities.get("version") === "10");
    };
    BrowserUtilitiesFactory.prototype.isFirefox = function () {
        return this._capabilities.get("browserName") === "firefox";
    };
    BrowserUtilitiesFactory.prototype.isEdge = function () {
        return this._capabilities.get("browserName") === "MicrosoftEdge";
    };
    BrowserUtilitiesFactory.prototype.isSafari = function () {
        return this._capabilities.get("browserName") === "safari";
    };
    return BrowserUtilitiesFactory;
}());

var CalendarUtilitiesFactory = /** @class */ (function () {
    function CalendarUtilitiesFactory(_browser, _by, _protractor) {
        this._browser = _browser;
        this._by = _by;
        this._protractor = _protractor;
        this.DF_DATEPICKER_ARROW = "df-datepicker__ctrl-btn df-datepicker__btn df-datepicker__ctrl-btn--";
    }
    CalendarUtilitiesFactory.prototype.getDateById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//df-datepicker[@id='" + elementId + "']"));
    };
    CalendarUtilitiesFactory.prototype.getTimeById = function (elementId) {
        return this._browser.driver.findElement(this._by.xpath("//df-timepicker[@id='" + elementId + "']"));
    };
    CalendarUtilitiesFactory.prototype.setDateById = function (elementId, value, direction) {
        var _this = this;
        if (direction === void 0) { direction = "previous"; }
        var currentYear = new Date().getFullYear();
        // tslint:disable-next-line:radix
        var years = currentYear - parseInt(value.substring(6, 11));
        var day = value.substring(0, 2);
        var datePicker = this._browser.driver.findElement(this._by.xpath("//df-datepicker[@id='" + elementId + "']//input"));
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10() || browserUtilities.isSafari()) {
                datePicker.click();
                var directionBtn = _this._browser.driver.findElement(_this._by.xpath("//df-datepicker[@id='" + elementId + "']//button[@class='" +
                    _this.DF_DATEPICKER_ARROW +
                    (direction + "']")));
                for (var i = 0; i < years * 12; i++) {
                    directionBtn.click();
                }
                var dayElement = _this._browser.driver.findElement(_this._by.xpath("//df-datepicker[@id='" +
                    elementId +
                    "']//span[contains(text(), '" +
                    day +
                    "')]"));
                dayElement.click();
            }
            else {
                datePicker.clear();
                datePicker.sendKeys(value);
                datePicker.sendKeys(_this._protractor.Key.ENTER);
            }
        });
    };
    return CalendarUtilitiesFactory;
}());

var DropdownUtilitiesFactory = /** @class */ (function () {
    function DropdownUtilitiesFactory(_browser, _by, _expectedConditions) {
        this._browser = _browser;
        this._by = _by;
        this._expectedConditions = _expectedConditions;
        this.DF_SELECT_OPTIONS_LIST = "df-select__options-list";
    }
    DropdownUtilitiesFactory.prototype.openDropdownByCss = function (selector) {
        this._browser.driver.executeScript("document.querySelector('" +
            selector +
            "').setAttribute('style', 'opacity: 1;');");
    };
    DropdownUtilitiesFactory.prototype.openDropdownById = function (id) {
        this._browser.driver.executeScript("document.querySelector('#" +
            id +
            " .df-select__options').setAttribute('style', 'opacity: 1;');");
    };
    DropdownUtilitiesFactory.prototype.closeDropdownByCss = function (selector) {
        this._browser.driver.executeScript("document.querySelector('" +
            selector +
            "').setAttribute('style', 'opacity: 0;');");
    };
    DropdownUtilitiesFactory.prototype.closeDropdownById = function (id) {
        this._browser.driver.executeScript("document.querySelector('#" +
            id +
            " .df-select__options').setAttribute('style', 'opacity: 0;');");
    };
    DropdownUtilitiesFactory.prototype.getDropdownById = function (elementId) {
        var path = this._by.xpath("//df-select[@id='" + elementId + "']");
        return this._browser.driver.findElement(path);
    };
    DropdownUtilitiesFactory.prototype.setDropdownOptionIndexById = function (elementId, optionIndex) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[" + optionIndex + "]");
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
        });
    };
    DropdownUtilitiesFactory.prototype.setDropdownValueById = function (elementId, value) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" +
                elementId +
                "']//df-option[@ng-reflect-value='" +
                value +
                "']");
            if (browserUtilities.isEdge() || browserUtilities.isFirefox()) {
                var optionsListPath = _this._by.xpath("//df-select[@id='" +
                    elementId +
                    "']//div[@class='" +
                    _this.DF_SELECT_OPTIONS_LIST +
                    "']");
                _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(optionsListPath)));
                var firstOptionPath = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[1]");
                _this._browser.wait(_this._expectedConditions.presenceOf(_this._browser.element(firstOptionPath)));
                _this.scrollIntoOption(elementId, value);
            }
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
        });
    };
    DropdownUtilitiesFactory.prototype.setMultiDropdownOptionIndexById = function (elementId, optionIndex) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[" + optionIndex + "]");
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
            else {
                _this._browser.driver
                    .findElement(_this._by.xpath("//df-select[@id='" + elementId + "']"))
                    .click();
            }
        });
    };
    DropdownUtilitiesFactory.prototype.setDropdownValueByIdWithoutSync = function (elementId, value) {
        var _this = this;
        this._browser.getCapabilities().then(function (cap) {
            var browserUtilities = new BrowserUtilitiesFactory(cap);
            if (browserUtilities.isIE10()) {
                _this.openDropdownById(elementId);
            }
            else {
                _this._browser.driver.findElement(_this._by.id(elementId)).click();
            }
            var path = _this._by.xpath("//df-select[@id='" +
                elementId +
                "']//df-option[@ng-reflect-value='" +
                value +
                "']");
            if (browserUtilities.isEdge() || browserUtilities.isFirefox()) {
                var optionsListPath = _this._by.xpath("//df-select[@id='" +
                    elementId +
                    "']//div[@class='" +
                    _this.DF_SELECT_OPTIONS_LIST +
                    "']");
                _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(optionsListPath)));
                var firstOptionPath = _this._by.xpath("//df-select[@id='" + elementId + "']//df-option[1]");
                _this._browser.wait(_this._expectedConditions.presenceOf(_this._browser.element(firstOptionPath)));
                _this.scrollIntoOption(elementId, value);
            }
            _this._browser.waitForAngularEnabled(false);
            _this._browser.wait(_this._expectedConditions.elementToBeClickable(_this._browser.element(path)), 5000);
            _this._browser.driver.findElement(path).click();
            if (browserUtilities.isIE10()) {
                _this.closeDropdownById(elementId);
            }
        });
    };
    DropdownUtilitiesFactory.prototype.scrollIntoOption = function (elementId, optionValue) {
        this._browser.driver.executeScript("document.querySelector(\"#" +
            elementId +
            " ." +
            this.DF_SELECT_OPTIONS_LIST +
            " [ng-reflect-value='" +
            optionValue +
            "']\").scrollIntoView()");
    };
    return DropdownUtilitiesFactory;
}());

var SidebarMenuComponentSelectorsFactory = /** @class */ (function () {
    function SidebarMenuComponentSelectorsFactory() {
    }
    SidebarMenuComponentSelectorsFactory.sideBarSubMenuItemXPath = function (text) {
        return this
            .selector + "//div[contains(@class,\"df-sidebar\")]//a[contains(normalize-space(.),\"" + text + "\")]";
    };
    SidebarMenuComponentSelectorsFactory.selector = "df-sidebar";
    return SidebarMenuComponentSelectorsFactory;
}());

/**
 * General purpose constants
 */
var ConstantsFactory = /** @class */ (function () {
    function ConstantsFactory() {
    }
    ConstantsFactory.stringEmpty = "";
    /**
     * Timeout collection to meet various needs
     * @type {{xs: number; s: number; m: number; l: number; xl: number; xxl: number; xxxl: number}}
     */
    ConstantsFactory.timeout = {
        xs: 2000,
        s: 5000,
        m: 10000,
        l: 25000,
        xl: 50000,
        xxl: 75000,
        xxxl: 200000
    };
    return ConstantsFactory;
}());

var ComponentHelpersFactory = /** @class */ (function () {
    function ComponentHelpersFactory() {
    }
    /**
     * Returns contains(text(),@param) or text()=@param based on the parameter
     *
     * @example
     * // Returns contains('.',attributeValue)
     * this.getXPathFunctionForStringComparison(text:attributeValue, attribute:'.', isContains:true);
     * // Returns normalize-space(.)=attributeValue
     * this.getXPathFunctionForStringComparison(text:attributeValue, attribute:'.', isContains:false);
     * @param {string} text
     * @param {string} attribute
     * @param {boolean} isContains
     * @returns {string} contains(text(),@param) or text()=@param based on the parameter
     */
    ComponentHelpersFactory.getXPathFunctionForStringComparison = function (text, attribute, isContains) {
        if (isContains === void 0) { isContains = false; }
        return isContains
            ? "contains(" + attribute + ",\"" + text + "\")"
            : "normalize-space(" + attribute + ")=\"" + text + "\"";
    };
    ComponentHelpersFactory.getXPathFunctionForDot = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getXPathFunctionForStringComparison(text, ".", isContains);
    };
    ComponentHelpersFactory.getXPathFunctionForText = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getXPathFunctionForStringComparison(text, "text()", isContains);
    };
    return ComponentHelpersFactory;
}());

var BreadcrumbsComponentSelectorsFactory = /** @class */ (function () {
    function BreadcrumbsComponentSelectorsFactory() {
    }
    Object.defineProperty(BreadcrumbsComponentSelectorsFactory, "linksXPath", {
        get: function () {
            return "//" + this.selectorTag + "//span//a";
        },
        enumerable: true,
        configurable: true
    });
    BreadcrumbsComponentSelectorsFactory.getBreadcrumbLinkXPath = function (label, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this
            .linksXPath + "[" + ComponentHelpersFactory.getXPathFunctionForText(label, isContains) + "]";
    };
    BreadcrumbsComponentSelectorsFactory.selectorTag = "df-breadcrumbs";
    return BreadcrumbsComponentSelectorsFactory;
}());

var ModelComponentSelectorsFactory = /** @class */ (function () {
    function ModelComponentSelectorsFactory() {
    }
    Object.defineProperty(ModelComponentSelectorsFactory, "modelPopupBodyXpath", {
        get: function () {
            return "//" + this.selector + "//div[contains(@class,'popover-body')]";
        },
        enumerable: true,
        configurable: true
    });
    ModelComponentSelectorsFactory.getModelPopupXpath = function (insidePopup) {
        return insidePopup ? "//" + this.selector : "";
    };
    ModelComponentSelectorsFactory.selector = "ngb-modal-window";
    return ModelComponentSelectorsFactory;
}());

var DropdownComponentSelectorsFactory = /** @class */ (function () {
    function DropdownComponentSelectorsFactory() {
    }
    DropdownComponentSelectorsFactory.getControlXPath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //label[" + ComponentHelpersFactory.getXPathFunctionForText(label, isContains) + "]\n        //following-sibling::" + this.selector;
    };
    DropdownComponentSelectorsFactory.getSelectedOptionXPath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getControlXPath(label, isContains, insidePopup) + "//*[contains(@class,'" + this.selector + "__input-text')]";
    };
    DropdownComponentSelectorsFactory.getOptionsXPath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getControlXPath(label, isContains, insidePopup) + "//*[contains(@class,'" + this.selector + "__options')]//df-option";
    };
    DropdownComponentSelectorsFactory.getOptionXPath = function (label, optionLabel, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getControlXPath(label, false, insidePopup) + "//*[contains(@class,'" + this.selector + "__options')]\n        //df-option[" + ComponentHelpersFactory.getXPathFunctionForText(optionLabel, isContains) + "]";
    };
    DropdownComponentSelectorsFactory.selector = "df-select";
    return DropdownComponentSelectorsFactory;
}());

var SectionComponentSelectorsFactory = /** @class */ (function () {
    function SectionComponentSelectorsFactory() {
    }
    SectionComponentSelectorsFactory.getControlXPath = function (title) {
        return "//h2[contains(@class,\"comp-section__header\") and normalize-space(.)=\"" + title + "\"]";
    };
    SectionComponentSelectorsFactory.getIconXPath = function (title, iconClass) {
        return this.getControlXPath(title) + "//following-sibling::div//button[i[contains(@class,\"" + iconClass + "\")]]";
    };
    return SectionComponentSelectorsFactory;
}());

var TabComponentSelectorsFactory = /** @class */ (function () {
    function TabComponentSelectorsFactory() {
    }
    TabComponentSelectorsFactory.activeTabWithTextXPath = function (sectionName, tab) {
        return (SectionComponentSelectorsFactory.getControlXPath(sectionName) + "\n        //*[contains(@class, \"link--active\")][normalize-space(.)=\"" +
            tab +
            "\"]");
    };
    return TabComponentSelectorsFactory;
}());

var TooltipComponentSelectorsFactory = /** @class */ (function () {
    function TooltipComponentSelectorsFactory() {
    }
    TooltipComponentSelectorsFactory.getTooltipXPath = function (message) {
        return "//" + this
            .selector + "//div[contains(@class,'tooltip-inner') and contains(normalize-space(.),'" + message + "')]";
    };
    TooltipComponentSelectorsFactory.selector = "ngb-modal-window";
    return TooltipComponentSelectorsFactory;
}());

var LoaderComponentSelectorsFactory = /** @class */ (function () {
    function LoaderComponentSelectorsFactory() {
    }
    LoaderComponentSelectorsFactory.getControlXPath = function () {
        return "//" + this.selector;
    };
    LoaderComponentSelectorsFactory.selector = "df-loading-spinner";
    return LoaderComponentSelectorsFactory;
}());

var TextBoxComponentSelectorsFactory = /** @class */ (function () {
    function TextBoxComponentSelectorsFactory() {
    }
    TextBoxComponentSelectorsFactory.getTextBoxByPlaceholderXpath = function (placeholder, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this
            .selector + "//input[normalize-space(@placeholder)=\"" + placeholder + "\"]";
    };
    TextBoxComponentSelectorsFactory.getErrorMessageInPopUp = function (formControlName) {
        return "//input[@formcontrolname=\"" + formControlName + "\"]/following-sibling::div[contains(@class,\"form__error\")]";
    };
    TextBoxComponentSelectorsFactory.getTextBoxByLabelXpath = function (label, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getXpathForForTextboxFollowingSiblingByLabel(label, insidePopup) + "input";
    };
    TextBoxComponentSelectorsFactory.getTextBoxErrorLabelXpath = function (label, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return this.getXpathForForTextboxFollowingSiblingByLabel(label, insidePopup) + "div[contains(@class,'form__error')]";
    };
    TextBoxComponentSelectorsFactory.getXpathForForTextboxFollowingSiblingByLabel = function (label, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this
            .selector + "//label[normalize-space(.)='" + label + "']//following-sibling::";
    };
    TextBoxComponentSelectorsFactory.getTextBoxByFormControlNameXpath = function (formControlName, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //*[@formcontrolname='" + formControlName + "']";
    };
    TextBoxComponentSelectorsFactory.selector = "df-input-container";
    return TextBoxComponentSelectorsFactory;
}());

var HtmlHelperFactory = /** @class */ (function () {
    function HtmlHelperFactory() {
    }
    Object.defineProperty(HtmlHelperFactory, "attributes", {
        get: function () {
            return {
                accept: "accept",
                acceptCharset: "accept-charset",
                accessKey: "accesskey",
                action: "action",
                align: "align",
                alt: "alt",
                async: "async",
                autoComplete: "autocomplete",
                autoFocus: "autofocus",
                autoPlay: "autoplay",
                bgColor: "bgcolor",
                border: "border",
                buffered: "buffered",
                challenge: "challenge",
                charset: "charset",
                checked: "checked",
                cite: "cite",
                class: "class",
                code: "code",
                codebase: "codebase",
                color: "color",
                cols: "cols",
                colspan: "colspan",
                content: "content",
                contentEditable: "contenteditable",
                contextMenu: "contextmenu",
                controls: "controls",
                coords: "coords",
                crossOrigin: "crossorigin",
                data: "data",
                datetime: "datetime",
                defaultt: "default",
                defer: "defer",
                dir: "dir",
                dirName: "dirname",
                disabled: "disabled",
                download: "download",
                draggable: "draggable",
                dropZone: "dropzone",
                enctype: "enctype",
                for: "for",
                form: "form",
                formControlName: "formcontrolname",
                formAction: "formaction",
                headers: "headers",
                height: "height",
                hidden: "hidden",
                high: "high",
                href: "href",
                hrefLang: "hreflang",
                httpEquiv: "http-equiv",
                icon: "icon",
                id: "id",
                integrity: "integrity",
                isMap: "ismap",
                itemProp: "itemprop",
                keyType: "keytype",
                kind: "kind",
                label: "label",
                lang: "lang",
                language: "language",
                list: "list",
                loop: "loop",
                low: "low",
                manifest: "manifest",
                max: "max",
                maxLength: "maxlength",
                minLength: "minlength",
                media: "media",
                method: "method",
                min: "min",
                multiple: "multiple",
                muted: "muted",
                name: "name",
                novalidate: "novalidate",
                open: "open",
                optimum: "optimum",
                pattern: "pattern",
                ping: "ping",
                placeholder: "placeholder",
                poster: "poster",
                preload: "preload",
                radioGroup: "radiogroup",
                readonly: "readonly",
                rel: "rel",
                required: "required",
                reversed: "reversed",
                rows: "rows",
                rowSpan: "rowspan",
                sandbox: "sandbox",
                scope: "scope",
                scoped: "scoped",
                seamless: "seamless",
                selected: "selected",
                shape: "shape",
                size: "size",
                sizes: "sizes",
                slot: "slot",
                span: "span",
                spellCheck: "spellcheck",
                src: "src",
                srcDoc: "srcdoc",
                srcLang: "srclang",
                srcset: "srcset",
                start: "start",
                step: "step",
                style: "style",
                summary: "summary",
                tabIndex: "tabindex",
                target: "target",
                title: "title",
                type: "type",
                useMap: "usemap",
                value: "value",
                width: "width",
                wrap: "wrap"
            };
        },
        enumerable: true,
        configurable: true
    });
    return HtmlHelperFactory;
}());

var ButtonComponentSelectorsFactory = /** @class */ (function () {
    function ButtonComponentSelectorsFactory() {
    }
    ButtonComponentSelectorsFactory.getButtonByFormControlNameXpath = function (formControlName, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this.selectorTag + "[@" + HtmlHelperFactory.attributes
            .formControlName + "='" + formControlName + "']";
    };
    ButtonComponentSelectorsFactory.getDropdownButtonByLabelXpath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //span[contains(@class,'df-button__content')\n        and\n        [" + ComponentHelpersFactory.getXPathFunctionForDot(label, isContains) + "]]//parent::button[@ngbdropdowntoggle]";
    };
    ButtonComponentSelectorsFactory.getDropdownButtonOptionsByLabelXpath = function (labelOfDropdownButton, labelOfOptionButton, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getDropdownButtonByLabelXpath(labelOfDropdownButton) + "\n        //following-sibling::div[@ngbdropdownmenu]//button[contains(@class,'dropdown-item')\n        and\n        " + ComponentHelpersFactory.getXPathFunctionForText(labelOfOptionButton, isContains) + "]";
    };
    ButtonComponentSelectorsFactory.getButtonByIconClassNameXpath = function (iconClass, insidePopup) {
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this.selectorTag + "[i[contains(@class,'" + iconClass + "')]]";
    };
    ButtonComponentSelectorsFactory.getButtonByLabelXpath = function (label, isContains, insidePopup) {
        if (isContains === void 0) { isContains = false; }
        if (insidePopup === void 0) { insidePopup = false; }
        return ModelComponentSelectorsFactory.getModelPopupXpath(insidePopup) + "\n        //" + this
            .selectorTag + "[span[" + ComponentHelpersFactory.getXPathFunctionForDot(label, isContains) + "]]";
    };
    ButtonComponentSelectorsFactory.selectorTag = "button";
    return ButtonComponentSelectorsFactory;
}());

var CalendarComponentSelectorsFactory = /** @class */ (function () {
    function CalendarComponentSelectorsFactory() {
    }
    CalendarComponentSelectorsFactory.getCreatedEventByTextXPath = function (calendarTitle, eventTitle, isContains) {
        if (isContains === void 0) { isContains = false; }
        return this.getCalendarByTitleXPath(calendarTitle) + "\n            //*[contains(@class,\"cal-event\") and contains(@class,\"event-type-event\")\n            and " + ComponentHelpersFactory.getXPathFunctionForText(eventTitle, isContains) + "]";
    };
    CalendarComponentSelectorsFactory.getCalendarByTitleXPath = function (calendarTitle) {
        return "//" + this
            .selector + "[" + SectionComponentSelectorsFactory.getControlXPath(calendarTitle) + "]";
    };
    CalendarComponentSelectorsFactory.getCalendarHeaderButtonByClassNameXPath = function (calendarTitle, className) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//button[contains(normalize-space(@class),'" + className + "')]";
    };
    CalendarComponentSelectorsFactory.getCalendarHeaderXPath = function (calendarTitle) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//app-cam-calendar-header";
    };
    CalendarComponentSelectorsFactory.getCalendarPreviousButtonXPath = function (calendarTitle) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//*[@mwlcalendarpreviousview]";
    };
    CalendarComponentSelectorsFactory.getCalendarNextButtonXPath = function (calendarTitle) {
        return this.getCalendarByTitleXPath(calendarTitle) + "//*[@mwlcalendarnextview]";
    };
    CalendarComponentSelectorsFactory.selector = "app-calendar";
    return CalendarComponentSelectorsFactory;
}());

var ToastComponentSelectorsFactory = /** @class */ (function () {
    function ToastComponentSelectorsFactory() {
    }
    Object.defineProperty(ToastComponentSelectorsFactory, "toastHeaderXPath", {
        get: function () {
            return this.toastBodyXPath + " > h1";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ToastComponentSelectorsFactory, "toastBodyXPath", {
        get: function () {
            return this.selector + " .df-toast__body";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ToastComponentSelectorsFactory, "toastContentXPath", {
        get: function () {
            return this.toastBodyXPath + " > p";
        },
        enumerable: true,
        configurable: true
    });
    ToastComponentSelectorsFactory.selector = "df-toast";
    return ToastComponentSelectorsFactory;
}());

var ButtonHelperFactory = /** @class */ (function () {
    function ButtonHelperFactory() {
    }
    ButtonHelperFactory.getButtonByExactTextXPath = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//button[" + ComponentHelpersFactory.getXPathFunctionForDot(text, isContains) + "]";
    };
    return ButtonHelperFactory;
}());

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */













function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

var WaitHelperFactory = /** @class */ (function () {
    function WaitHelperFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
        this.EC = this._browser.ExpectedConditions;
        this.timeoutMessage = " not found in time";
        this.DEFAULT_TIMEOUT = ConstantsFactory.timeout.xxl;
    }
    /**
     * Default timeout for promises
     * @type {number}
     */
    /**
     * Wait for an element to exist
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    WaitHelperFactory.prototype.waitForElement = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.presenceOf, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Wait for an element to display
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    WaitHelperFactory.prototype.waitForElementToBeDisplayed = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.visibilityOf, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Wait for an element to hide
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     * @returns {any}
     */
    WaitHelperFactory.prototype.waitForElementToBeHidden = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.invisibilityOf, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Wait for an element to become clickable
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     */
    WaitHelperFactory.prototype.waitForElementToBeClickable = function (targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.waiter(this.EC.elementToBeClickable, targetElement, timeout, message)];
            });
        });
    };
    /**
     * Common waiter
     * @param {Function} functionName
     * @param {any} targetElement
     * @param {number} timeout
     * @param {string} message
     * @returns {Promise<void>}
     */
    WaitHelperFactory.prototype.waiter = function (functionName, targetElement, timeout, message) {
        if (timeout === void 0) { timeout = this.DEFAULT_TIMEOUT; }
        if (message === void 0) { message = ""; }
        return __awaiter(this, void 0, void 0, function () {
            var waiterResponsible;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        waiterResponsible = functionName(targetElement);
                        if (message.length === 0) {
                            message = targetElement.locator().toString() + this.timeoutMessage;
                        }
                        return [4 /*yield*/, this._browser.wait(waiterResponsible, timeout, message)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return WaitHelperFactory;
}());

/**
 * Page helper for general utility
 */
var PageHelperFactory = /** @class */ (function () {
    function PageHelperFactory(_browser, _by) {
        this._browser = _browser;
        this._by = _by;
        this.waitHelperFactory = new WaitHelperFactory(this._browser, this._by);
    }
    // Known issue for chrome, direct maximize window doesn't work
    /**
     * To maximize the this._browser window
     */
    PageHelperFactory.prototype.maximizeWindow = function () {
        return __awaiter(this, void 0, void 0, function () {
            var Size, windowSize, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        
                        return [4 /*yield*/, this.executeScript(function () {
                                return {
                                    width: window.screen.availWidth,
                                    height: window.screen.availHeight
                                };
                            })];
                    case 1:
                        windowSize = _a.sent();
                        result = windowSize;
                        return [2 /*return*/, this.setWindowSize(result.width, result.height)];
                }
            });
        });
    };
    /**
     * Sets window size
     * @param {number} width
     * @param {number} height
     */
    PageHelperFactory.prototype.setWindowSize = function (width, height) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._browser.driver
                        .manage()
                        .window()
                        .setSize(width, height)];
            });
        });
    };
    /**
     * Wrapper for executing javascript code
     * @param {string | Function} script
     * @param varAargs
     * @returns {promise.Promise<any>}
     */
    PageHelperFactory.prototype.executeScript = function (script) {
        var varAargs = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            varAargs[_i - 1] = arguments[_i];
        }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._browser.driver.executeScript(script, varAargs)];
            });
        });
    };
    /**
     * Wrapper to return an active element
     * @returns {any}

     public  async getFocusedElement() {
    return this._browser.driver.switchTo().activeElement()
  }*/
    /**
     * Switch to a new tab if this._browser has availability
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    PageHelperFactory.prototype.switchToNewTabIfAvailable = function () {
        return __awaiter(this, void 0, void 0, function () {
            var handles, newWindowHandle, url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._browser.getAllWindowHandles()];
                    case 1:
                        handles = _a.sent();
                        newWindowHandle = handles[1];
                        if (!newWindowHandle) return [3 /*break*/, 3];
                        return [4 /*yield*/, this._browser.switchTo().window(newWindowHandle)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [4 /*yield*/, this._browser.getCurrentUrl()];
                    case 4:
                        url = _a.sent();
                        // Avoiding bootstraping issue, Known issue
                        // Error: Error while waiting for Protractor to sync with the page:
                        // "window.angular is undefined. This could be either because this is a non-angular page or
                        // because your test involves client-side navigation, which can interfere with Protractor's bootstrapping.
                        // See http://git.io/v4gXM for details
                        return [2 /*return*/, this._browser.driver.get(url)];
                }
            });
        });
    };
    /**
     * Gets html attribute value
     * @param {WebElementPromise} elem
     * @param {string} attribute
     * @returns {string} attribute value
     */
    PageHelperFactory.prototype.getAttributeValue = function (elem, attribute) {
        return __awaiter(this, void 0, void 0, function () {
            var attributeValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, elem.getAttribute(attribute)];
                    case 1:
                        attributeValue = _a.sent();
                        return [2 /*return*/, attributeValue.trim()];
                }
            });
        });
    };
    /**
     * Click on element
     * @param {any} targetElement
     * @returns {any}
     */
    PageHelperFactory.prototype.click = function (targetElement) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitHelperFactory.waitForElementToBeClickable(targetElement)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, targetElement.click()];
                }
            });
        });
    };
    /**
     * Click on the element and wait for it to get hidden
     * @param {any} targetElement
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    PageHelperFactory.prototype.clickAndWaitForElementToHide = function (targetElement) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.waitHelperFactory.waitForElementToBeClickable(targetElement)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, targetElement.click()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, this.waitHelperFactory.waitForElementToBeHidden(targetElement)];
                }
            });
        });
    };
    /**
     * Gets promise for current url
     * @returns {any}
     */
    PageHelperFactory.prototype.currentUrl = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._browser.getCurrentUrl()];
            });
        });
    };
    return PageHelperFactory;
}());

var RadioComponentSelectorsFactory = /** @class */ (function () {
    function RadioComponentSelectorsFactory() {
    }
    RadioComponentSelectorsFactory.getDfOptionsByTextXPath = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this.selector + ComponentHelpersFactory.getXPathFunctionForDot(text, isContains);
    };
    RadioComponentSelectorsFactory.selector = "df-option";
    return RadioComponentSelectorsFactory;
}());

var LabelComponentSelectorsFactory = /** @class */ (function () {
    function LabelComponentSelectorsFactory() {
    }
    LabelComponentSelectorsFactory.getInfoIconXPath = function (labelName) {
        return "//" + this
            .selector + "[normalize-space(.)=\"" + labelName + "\"]/following-sibling::i";
    };
    LabelComponentSelectorsFactory.selector = "label";
    return LabelComponentSelectorsFactory;
}());

var CheckboxComponentSelectorsFactory = /** @class */ (function () {
    function CheckboxComponentSelectorsFactory() {
    }
    CheckboxComponentSelectorsFactory.getControlXpath = function (label, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this
            .selector + "//span[" + ComponentHelpersFactory.getXPathFunctionForText(label, isContains) + "]\n        //preceding-sibling::" + this.checkboxBoxIcon;
    };
    CheckboxComponentSelectorsFactory.getCheckboxByFormControlNameXpath = function (name) {
        return "//df-checkbox[@formcontrolname=\"" + name + "\"]" + this.checkboxBoxIcon;
    };
    CheckboxComponentSelectorsFactory.selector = "df-checkbox";
    CheckboxComponentSelectorsFactory.checkboxBoxIcon = "span[@class=\"df-checkbox__icon\"]/i";
    return CheckboxComponentSelectorsFactory;
}());

var GridComponentSelectorsFactory = /** @class */ (function () {
    function GridComponentSelectorsFactory() {
    }
    GridComponentSelectorsFactory.getCellByText = function (text, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this.selector + "//div[contains(@class,\"ngCellText\")]\n            //span[" + ComponentHelpersFactory.getXPathFunctionForText(text, isContains) + "]";
    };
    GridComponentSelectorsFactory.getColumnItemXPath = function (columnName, listName, isContains) {
        if (isContains === void 0) { isContains = false; }
        return "//" + this.selector + "//td[@data-header=\"" + columnName + "\"]\n        //div[" + ComponentHelpersFactory.getXPathFunctionForDot(listName, isContains) + "]";
    };
    GridComponentSelectorsFactory.getSelectItemCheckBoxXPath = function (dataHeaderName, dataHeaderItemName) {
        return "//" + this
            .selector + "//td[@data-header=\"" + dataHeaderName + "\"]/div[contains(normalize-space(.),\"" + dataHeaderItemName + "\")]\n        /parent::td/preceding-sibling::td//i[contains(@class,\"fa-square\")]";
    };
    GridComponentSelectorsFactory.columnItemButtonXPath = function (columnName, listName, buttonName, isContainsInButtonName) {
        if (isContainsInButtonName === void 0) { isContainsInButtonName = false; }
        return "//" + this
            .selector + "//td[@data-header=\"" + columnName + "\"]//div[contains(normalize-space(.),\"" + listName + "\")]\n        //following-sibling::div//a[" + ComponentHelpersFactory.getXPathFunctionForDot(buttonName, isContainsInButtonName) + "]";
    };
    GridComponentSelectorsFactory.getDropDownOptionsXPath = function (dropDownName) {
        return "//" + this.selector + "//p[normalize-space(text())=\"" + dropDownName + "\"]\n        /following-sibling::ul//span[@class=\"" + CheckboxComponentSelectorsFactory.selector + "__label\"]";
    };
    GridComponentSelectorsFactory.getDropDownOptionXPath = function (dropDownName, optionName) {
        return "//" + this.getDropDownOptionsXPath(dropDownName) + "[normalize-space(.)=\"" + optionName + "\"]";
    };
    GridComponentSelectorsFactory.selector = "table[@df-table]";
    return GridComponentSelectorsFactory;
}());

var CommonLabelFactory = /** @class */ (function () {
    function CommonLabelFactory() {
    }
    CommonLabelFactory.save = "Save";
    CommonLabelFactory.ok = "Ok";
    CommonLabelFactory.saveAs = "Save As";
    CommonLabelFactory.saveAsWithDots = "Save as...";
    CommonLabelFactory.done = "Done";
    CommonLabelFactory.cancel = "Cancel";
    CommonLabelFactory.add = "Add";
    CommonLabelFactory.edit = "Edit";
    CommonLabelFactory.yes = "Yes";
    CommonLabelFactory.no = "No";
    return CommonLabelFactory;
}());

var FontAwesomeFactory = /** @class */ (function () {
    function FontAwesomeFactory() {
    }
    Object.defineProperty(FontAwesomeFactory, "IconClasses", {
        get: function () {
            return {
                accusoft: "fa-accusoft",
                accessibleIcon: "fa-accessible-icon",
                addressBook: "fa-address-book",
                addressCard: "fa-address-card",
                adjust: "fa-adjust",
                adn: "fa-adn",
                adversal: "fa-adversal",
                affiliateTheme: "fa-affiliatetheme",
                alarmClock: "fa-alarm-clock",
                algolia: "fa-algolia",
                alignCenter: "fa-align-center",
                alignJustify: "fa-align-justify",
                alignLeft: "fa-align-left",
                alignRight: "fa-align-right",
                amazon: "fa-amazon",
                amazonPay: "fa-amazon-pay",
                ambulance: "fa-ambulance",
                americanSignLanguageInterpreting: "fa-american-sign-language-interpreting",
                amilia: "fa-amilia",
                anchor: "fa-anchor",
                android: "fa-android",
                angellist: "fa-angellist",
                angleDoubleDown: "fa-angle-double-down",
                angleDoubleLeft: "fa-angle-double-left",
                angleDoubleRight: "fa-angle-double-right",
                angleDoubleUp: "fa-angle-double-up",
                angleDown: "fa-angle-down",
                angleLeft: "fa-angle-left",
                angleRight: "fa-angle-right",
                angleUp: "fa-angle-up",
                angrycreative: "fa-angrycreative",
                angular: "fa-angular",
                appStore: "fa-app-store",
                appStoreIos: "fa-app-store-ios",
                apper: "fa-apper",
                apple: "fa-apple",
                applePay: "fa-apple-pay",
                archive: "fa-archive",
                arrowAltCircleDown: "fa-arrow-alt-circle-down",
                arrowAltCircleLeft: "fa-arrow-alt-circle-left",
                arrowAltCircleRight: "fa-arrow-alt-circle-right",
                arrowAltCircleUp: "fa-arrow-alt-circle-up",
                arrowAltDown: "fa-arrow-alt-down",
                arrowAltFromBottom: "fa-arrow-alt-from-bottom",
                arrowAltFromLeft: "fa-arrow-alt-from-left",
                arrowAltFromRight: "fa-arrow-alt-from-right",
                arrowAltFromTop: "fa-arrow-alt-from-top",
                arrowAltLeft: "fa-arrow-alt-left",
                arrowAltRight: "fa-arrow-alt-right",
                arrowAltSquareDown: "fa-arrow-alt-square-down",
                arrowAltSquareLeft: "fa-arrow-alt-square-left",
                arrowAltSquareRight: "fa-arrow-alt-square-right",
                arrowAltSquareUp: "fa-arrow-alt-square-up",
                arrowAltToBottom: "fa-arrow-alt-to-bottom",
                arrowAltToLeft: "fa-arrow-alt-to-left",
                arrowAltToRight: "fa-arrow-alt-to-right",
                arrowAltToTop: "fa-arrow-alt-to-top",
                arrowAltUp: "fa-arrow-alt-up",
                arrowCircleDown: "fa-arrow-circle-down",
                arrowCircleLeft: "fa-arrow-circle-left",
                arrowCircleRight: "fa-arrow-circle-right",
                arrowCircleUp: "fa-arrow-circle-up",
                arrowDown: "fa-arrow-down",
                arrowFromBottom: "fa-arrow-from-bottom",
                arrowFromLeft: "fa-arrow-from-left",
                arrowFromRight: "fa-arrow-from-right",
                arrowFromTop: "fa-arrow-from-top",
                arrowLeft: "fa-arrow-left",
                arrowRight: "fa-arrow-right",
                arrowSquareDown: "fa-arrow-square-down",
                arrowSquareLeft: "fa-arrow-square-left",
                arrowSquareRight: "fa-arrow-square-right",
                arrowSquareUp: "fa-arrow-square-up",
                arrowToBottom: "fa-arrow-to-bottom",
                arrowToLeft: "fa-arrow-to-left",
                arrowToRight: "fa-arrow-to-right",
                arrowToTop: "fa-arrow-to-top",
                arrowUp: "fa-arrow-up",
                arrows: "fa-arrows",
                arrowsAlt: "fa-arrows-alt",
                arrowsAltH: "fa-arrows-alt-h",
                arrowsAltV: "fa-arrows-alt-v",
                arrowsH: "fa-arrows-h",
                arrowsV: "fa-arrows-v",
                assistiveListeningSystems: "fa-assistive-listening-systems",
                asterisk: "fa-asterisk",
                asymmetrik: "fa-asymmetrik",
                at: "fa-at",
                audible: "fa-audible",
                audioDescription: "fa-audio-description",
                autoprefixer: "fa-autoprefixer",
                avianex: "fa-avianex",
                aviato: "fa-aviato",
                aws: "fa-aws",
                backward: "fa-backward",
                badge: "fa-badge",
                badgeCheck: "fa-badge-check",
                balanceScale: "fa-balance-scale",
                ban: "fa-ban",
                bandcamp: "fa-bandcamp",
                barcode: "fa-barcode",
                bars: "fa-bars",
                bath: "fa-bath",
                batteryBolt: "fa-battery-bolt",
                batteryEmpty: "fa-battery-empty",
                batteryFull: "fa-battery-full",
                batteryHalf: "fa-battery-half",
                batteryQuarter: "fa-battery-quarter",
                batterySlash: "fa-battery-slash",
                batteryThreeQuarters: "fa-battery-three-quarters",
                bed: "fa-bed",
                beer: "fa-beer",
                behance: "fa-behance",
                behanceSquare: "fa-behance-square",
                bell: "fa-bell",
                bellSlash: "fa-bell-slash",
                bicycle: "fa-bicycle",
                bimobject: "fa-bimobject",
                binoculars: "fa-binoculars",
                birthdayCake: "fa-birthday-cake",
                bitbucket: "fa-bitbucket",
                bitcoin: "fa-bitcoin",
                bity: "fa-bity",
                blackTie: "fa-black-tie",
                blackberry: "fa-blackberry",
                blind: "fa-blind",
                blogger: "fa-blogger",
                bloggerB: "fa-blogger-b",
                bluetooth: "fa-bluetooth",
                bluetoothB: "fa-bluetooth-b",
                bold: "fa-bold",
                bolt: "fa-bolt",
                bomb: "fa-bomb",
                book: "fa-book",
                bookmark: "fa-bookmark",
                braille: "fa-braille",
                briefcase: "fa-briefcase",
                browser: "fa-browser",
                btc: "fa-btc",
                bug: "fa-bug",
                building: "fa-building",
                bullhorn: "fa-bullhorn",
                bullseye: "fa-bullseye",
                buromobelexperte: "fa-buromobelexperte",
                bus: "fa-bus",
                buysellads: "fa-buysellads",
                calculator: "fa-calculator",
                calendar: "fa-calendar",
                calendarAlt: "fa-calendar-alt",
                calendarCheck: "fa-calendar-check",
                calendarEdit: "fa-calendar-edit",
                calendarExclamation: "fa-calendar-exclamation",
                calendarMinus: "fa-calendar-minus",
                calendarPlus: "fa-calendar-plus",
                calendarTimes: "fa-calendar-times",
                camera: "fa-camera",
                cameraAlt: "fa-camera-alt",
                cameraRetro: "fa-camera-retro",
                car: "fa-car",
                caretCircleDown: "fa-caret-circle-down",
                caretCircleLeft: "fa-caret-circle-left",
                caretCircleRight: "fa-caret-circle-right",
                caretCircleUp: "fa-caret-circle-up",
                caretDown: "fa-caret-down",
                caretLeft: "fa-caret-left",
                caretRight: "fa-caret-right",
                caretSquareDown: "fa-caret-square-down",
                caretSquareLeft: "fa-caret-square-left",
                caretSquareRight: "fa-caret-square-right",
                caretSquareUp: "fa-caret-square-up",
                caretUp: "fa-caret-up",
                cartArrowDown: "fa-cart-arrow-down",
                cartPlus: "fa-cart-plus",
                ccAmazonPay: "fa-cc-amazon-pay",
                ccAmex: "fa-cc-amex",
                ccApplePay: "fa-cc-apple-pay",
                ccDinersClub: "fa-cc-diners-club",
                ccDiscover: "fa-cc-discover",
                ccJcb: "fa-cc-jcb",
                ccMastercard: "fa-cc-mastercard",
                ccPaypal: "fa-cc-paypal",
                ccStripe: "fa-cc-stripe",
                ccVisa: "fa-cc-visa",
                centercode: "fa-centercode",
                certificate: "fa-certificate",
                chartArea: "fa-chart-area",
                chartBar: "fa-chart-bar",
                chartLine: "fa-chart-line",
                chartPie: "fa-chart-pie",
                check: "fa-check",
                checkCircle: "fa-check-circle",
                checkSquare: "fa-check-square",
                chevronCircleDown: "fa-chevron-circle-down",
                chevronCircleLeft: "fa-chevron-circle-left",
                chevronCircleRight: "fa-chevron-circle-right",
                chevronCircleUp: "fa-chevron-circle-up",
                chevronDoubleDown: "fa-chevron-double-down",
                chevronDoubleLeft: "fa-chevron-double-left",
                chevronDoubleRight: "fa-chevron-double-right",
                chevronDoubleUp: "fa-chevron-double-up",
                chevronDown: "fa-chevron-down",
                chevronLeft: "fa-chevron-left",
                chevronRight: "fa-chevron-right",
                chevronSquareDown: "fa-chevron-square-down",
                chevronSquareLeft: "fa-chevron-square-left",
                chevronSquareRight: "fa-chevron-square-right",
                chevronSquareUp: "fa-chevron-square-up",
                chevronUp: "fa-chevron-up",
                child: "fa-child",
                chrome: "fa-chrome",
                circle: "fa-circle",
                circleNotch: "fa-circle-notch",
                clipboard: "fa-clipboard",
                clock: "fa-clock",
                clone: "fa-clone",
                closedCaptioning: "fa-closed-captioning",
                cloud: "fa-cloud",
                cloudDownload: "fa-cloud-download",
                cloudDownloadAlt: "fa-cloud-download-alt",
                cloudUpload: "fa-cloud-upload",
                cloudUploadAlt: "fa-cloud-upload-alt",
                cloudScale: "fa-cloudscale",
                cloudSmith: "fa-cloudsmith",
                cloudVersify: "fa-cloudversify",
                club: "fa-club",
                code: "fa-code",
                codeBranch: "fa-code-branch",
                codeCommit: "fa-code-commit",
                codeMerge: "fa-code-merge",
                codepen: "fa-codepen",
                codiepie: "fa-codiepie",
                coffee: "fa-coffee",
                cog: "fa-cog",
                cogs: "fa-cogs",
                columns: "fa-columns",
                comment: "fa-comment",
                commentAlt: "fa-comment-alt",
                comments: "fa-comments",
                compass: "fa-compass",
                compress: "fa-compress",
                compressAlt: "fa-compress-alt",
                compressWide: "fa-compress-wide",
                connectDevelop: "fa-connectdevelop",
                contao: "fa-contao",
                copy: "fa-copy",
                copyright: "fa-copyright",
                cPanel: "fa-cpanel",
                creativeCommons: "fa-creative-commons",
                creditCard: "fa-credit-card",
                creditCardBlank: "fa-credit-card-blank",
                creditCardFront: "fa-credit-card-front",
                crop: "fa-crop",
                crossHairs: "fa-crosshairs",
                css3: "fa-css3",
                css3Alt: "fa-css3-alt",
                cube: "fa-cube",
                cubes: "fa-cubes",
                cut: "fa-cut",
                cuttlefish: "fa-cuttlefish",
                dAndD: "fa-d-and-d",
                dashCube: "fa-dashcube",
                database: "fa-database",
                deaf: "fa-deaf",
                delicious: "fa-delicious",
                deployDog: "fa-deploydog",
                deskPro: "fa-deskpro",
                desktop: "fa-desktop",
                desktopAlt: "fa-desktop-alt",
                deviantArt: "fa-deviantart",
                diamond: "fa-diamond",
                digg: "fa-digg",
                digitalOcean: "fa-digital-ocean",
                discord: "fa-discord",
                discourse: "fa-discourse",
                docHub: "fa-dochub",
                docker: "fa-docker",
                dollarSign: "fa-dollar-sign",
                dotCircle: "fa-dot-circle",
                download: "fa-download",
                draft2digital: "fa-draft2digital",
                dribbble: "fa-dribbble",
                dribbbleSquare: "fa-dribbble-square",
                dropbox: "fa-dropbox",
                drupal: "fa-drupal",
                dyalog: "fa-dyalog",
                earlyBirds: "fa-earlybirds",
                edge: "fa-edge",
                edit: "fa-edit",
                eject: "fa-eject",
                elementor: "fa-elementor",
                ellipsisH: "fa-ellipsis-h",
                ellipsisHorizontalAlt: "fa-ellipsis-h-alt",
                ellipsisV: "fa-ellipsis-v",
                ellipsisVerticalAlt: "fa-ellipsis-v-alt",
                ember: "fa-ember",
                empire: "fa-empire",
                envelope: "fa-envelope",
                envelopeOpen: "fa-envelope-open",
                envelopeSquare: "fa-envelope-square",
                envira: "fa-envira",
                eraser: "fa-eraser",
                erlang: "fa-erlang",
                ethereum: "fa-ethereum",
                etsy: "fa-etsy",
                euroSign: "fa-euro-sign",
                exchange: "fa-exchange",
                exchangeAlt: "fa-exchange-alt",
                exclamation: "fa-exclamation",
                exclamationCircle: "fa-exclamation-circle",
                exclamationSquare: "fa-exclamation-square",
                exclamationTriangle: "fa-exclamation-triangle",
                expand: "fa-expand",
                expandAlt: "fa-expand-alt",
                expandArrows: "fa-expand-arrows",
                expandArrowsAlt: "fa-expand-arrows-alt",
                expandWide: "fa-expand-wide",
                expeditedssl: "fa-expeditedssl",
                externalLink: "fa-external-link",
                externalLinkAlt: "fa-external-link-alt",
                externalLinkSquare: "fa-external-link-square",
                externalLinkSquareAlt: "fa-external-link-square-alt",
                eye: "fa-eye",
                eyeDropper: "fa-eye-dropper",
                eyeSlash: "fa-eye-slash",
                facebook: "fa-facebook",
                facebookF: "fa-facebook-f",
                facebookMessenger: "fa-facebook-messenger",
                facebookSquare: "fa-facebook-square",
                fastBackward: "fa-fast-backward",
                fastForward: "fa-fast-forward",
                fax: "fa-fax",
                female: "fa-female",
                fighterJet: "fa-fighter-jet",
                file: "fa-file",
                fileAlt: "fa-file-alt",
                fileArchive: "fa-file-archive",
                fileAudio: "fa-file-audio",
                fileCheck: "fa-file-check",
                fileCode: "fa-file-code",
                fileEdit: "fa-file-edit",
                fileExcel: "fa-file-excel",
                fileExclamation: "fa-file-exclamation",
                fileImage: "fa-file-image",
                fileMinus: "fa-file-minus",
                filePdf: "fa-file-pdf",
                filePlus: "fa-file-plus",
                filePowerpoint: "fa-file-powerpoint",
                fileTimes: "fa-file-times",
                fileVideo: "fa-file-video",
                fileWord: "fa-file-word",
                film: "fa-film",
                filmAlt: "fa-film-alt",
                filter: "fa-filter",
                fire: "fa-fire",
                fireExtinguisher: "fa-fire-extinguisher",
                firefox: "fa-firefox",
                firstOrder: "fa-first-order",
                firstDraft: "fa-firstdraft",
                flag: "fa-flag",
                flagCheckered: "fa-flag-checkered",
                flask: "fa-flask",
                flickr: "fa-flickr",
                fly: "fa-fly",
                folder: "fa-folder",
                folderOpen: "fa-folder-open",
                font: "fa-font",
                fontAwesome: "fa-font-awesome",
                fontAwesomeAlt: "fa-font-awesome-alt",
                fontAwesomeFlag: "fa-font-awesome-flag",
                fontIcons: "fa-fonticons",
                fontIconsFi: "fa-fonticons-fi",
                fortAwesome: "fa-fort-awesome",
                fortAwesomeAlt: "fa-fort-awesome-alt",
                forumBee: "fa-forumbee",
                forward: "fa-forward",
                foursquare: "fa-foursquare",
                freeCodeCamp: "fa-free-code-camp",
                freeBsd: "fa-freebsd",
                frown: "fa-frown",
                futbol: "fa-futbol",
                gamePad: "fa-gamepad",
                gavel: "fa-gavel",
                gem: "fa-gem",
                genderless: "fa-genderless",
                getPocket: "fa-get-pocket",
                gg: "fa-gg",
                ggCircle: "fa-gg-circle",
                gift: "fa-gift",
                git: "fa-git",
                gitSquare: "fa-git-square",
                github: "fa-github",
                githubAlt: "fa-github-alt",
                githubSquare: "fa-github-square",
                gitkraken: "fa-gitkraken",
                gitlab: "fa-gitlab",
                gitter: "fa-gitter",
                glassMartini: "fa-glass-martini",
                glide: "fa-glide",
                glideG: "fa-glide-g",
                globe: "fa-globe",
                gofore: "fa-gofore",
                goodReads: "fa-goodreads",
                goodReadsG: "fa-goodreads-g",
                google: "fa-google",
                googleDrive: "fa-google-drive",
                googlePlay: "fa-google-play",
                googlePlus: "fa-google-plus",
                googlePlusG: "fa-google-plus-g",
                googlePlusSquare: "fa-google-plus-square",
                googleWallet: "fa-google-wallet",
                graduationCap: "fa-graduation-cap",
                gratiPay: "fa-gratipay",
                grav: "fa-grav",
                gripFire: "fa-gripfire",
                grunt: "fa-grunt",
                gulp: "fa-gulp",
                hSquare: "fa-h-square",
                h1: "fa-h1",
                h2: "fa-h2",
                h3: "fa-h3",
                hackerNews: "fa-hacker-news",
                hackerNewsSquare: "fa-hacker-news-square",
                handLizard: "fa-hand-lizard",
                handPaper: "fa-hand-paper",
                handPeace: "fa-hand-peace",
                handPointDown: "fa-hand-point-down",
                handPointLeft: "fa-hand-point-left",
                handPointRight: "fa-hand-point-right",
                handPointUp: "fa-hand-point-up",
                handPointer: "fa-hand-pointer",
                handRock: "fa-hand-rock",
                handScissors: "fa-hand-scissors",
                handSpock: "fa-hand-spock",
                handshake: "fa-handshake",
                hashtag: "fa-hashtag",
                hdd: "fa-hdd",
                heading: "fa-heading",
                headphones: "fa-headphones",
                heart: "fa-heart",
                heartbeat: "fa-heartbeat",
                hexagon: "fa-hexagon",
                hireAHelper: "fa-hire-a-helper",
                history: "fa-history",
                home: "fa-home",
                hooli: "fa-hooli",
                hospital: "fa-hospital",
                hotJar: "fa-hotjar",
                hourglass: "fa-hourglass",
                hourglassEnd: "fa-hourglass-end",
                hourglassHalf: "fa-hourglass-half",
                hourglassStart: "fa-hourglass-start",
                houzz: "fa-houzz",
                html5: "fa-html5",
                hubSpot: "fa-hubspot",
                iCursor: "fa-i-cursor",
                idBadge: "fa-id-badge",
                idCard: "fa-id-card",
                image: "fa-image",
                images: "fa-images",
                imdb: "fa-imdb",
                inbox: "fa-inbox",
                inboxIn: "fa-inbox-in",
                inboxOut: "fa-inbox-out",
                indent: "fa-indent",
                industry: "fa-industry",
                industryAlt: "fa-industry-alt",
                info: "fa-info",
                infoCircle: "fa-info-circle",
                infoSquare: "fa-info-square",
                instagram: "fa-instagram",
                internetExplorer: "fa-internet-explorer",
                ioxhost: "fa-ioxhost",
                italic: "fa-italic",
                itunes: "fa-itunes",
                itunesNote: "fa-itunes-note",
                jackOLantern: "fa-jack-o-lantern",
                jenkins: "fa-jenkins",
                joget: "fa-joget",
                joomla: "fa-joomla",
                js: "fa-js",
                jsSquare: "fa-js-square",
                jsFiddle: "fa-jsfiddle",
                key: "fa-key",
                keyboard: "fa-keyboard",
                keycdn: "fa-keycdn",
                kickStarter: "fa-kickstarter",
                kickStarterK: "fa-kickstarter-k",
                korvue: "fa-korvue",
                language: "fa-language",
                laptop: "fa-laptop",
                laravel: "fa-laravel",
                lastFm: "fa-lastfm",
                lastFmSquare: "fa-lastfm-square",
                leaf: "fa-leaf",
                leanpub: "fa-leanpub",
                lemon: "fa-lemon",
                less: "fa-less",
                levelDown: "fa-level-down",
                levelDownAlt: "fa-level-down-alt",
                levelUp: "fa-level-up",
                levelUpAlt: "fa-level-up-alt",
                lifeRing: "fa-life-ring",
                lightBulb: "fa-lightbulb",
                line: "fa-line",
                link: "fa-link",
                linkedIn: "fa-linkedin",
                linkedInIn: "fa-linkedin-in",
                linode: "fa-linode",
                linux: "fa-linux",
                liraSign: "fa-lira-sign",
                list: "fa-list",
                listAlt: "fa-list-alt",
                listOl: "fa-list-ol",
                listUl: "fa-list-ul",
                locationArrow: "fa-location-arrow",
                lock: "fa-lock",
                lockAlt: "fa-lock-alt",
                lockOpen: "fa-lock-open",
                lockOpenAlt: "fa-lock-open-alt",
                longArrowAltDown: "fa-long-arrow-alt-down",
                longArrowAltLeft: "fa-long-arrow-alt-left",
                longArrowAltRight: "fa-long-arrow-alt-right",
                longArrowAltUp: "fa-long-arrow-alt-up",
                longArrowDown: "fa-long-arrow-down",
                longArrowLeft: "fa-long-arrow-left",
                longArrowRight: "fa-long-arrow-right",
                longArrowUp: "fa-long-arrow-up",
                lowVision: "fa-low-vision",
                lyft: "fa-lyft",
                magento: "fa-magento",
                magic: "fa-magic",
                magnet: "fa-magnet",
                male: "fa-male",
                map: "fa-map",
                mapMarker: "fa-map-marker",
                mapMarkerAlt: "fa-map-marker-alt",
                mapPin: "fa-map-pin",
                mapSigns: "fa-map-signs",
                mars: "fa-mars",
                marsDouble: "fa-mars-double",
                marsStroke: "fa-mars-stroke",
                marsStrokeH: "fa-mars-stroke-h",
                marsStrokeV: "fa-mars-stroke-v",
                maxcdn: "fa-maxcdn",
                medapps: "fa-medapps",
                medium: "fa-medium",
                mediumM: "fa-medium-m",
                medkit: "fa-medkit",
                medrt: "fa-medrt",
                meetUp: "fa-meetup",
                meh: "fa-meh",
                mercury: "fa-mercury",
                microchip: "fa-microchip",
                microphone: "fa-microphone",
                microphoneAlt: "fa-microphone-alt",
                microphoneSlash: "fa-microphone-slash",
                microsoft: "fa-microsoft",
                minus: "fa-minus",
                minusCircle: "fa-minus-circle",
                minusHexagon: "fa-minus-hexagon",
                minusOctagon: "fa-minus-octagon",
                minusSquare: "fa-minus-square",
                mix: "fa-mix",
                mixCloud: "fa-mixcloud",
                mizuni: "fa-mizuni",
                mobile: "fa-mobile",
                mobileAlt: "fa-mobile-alt",
                mobileAndroid: "fa-mobile-android",
                mobileAndroidAlt: "fa-mobile-android-alt",
                modx: "fa-modx",
                monero: "fa-monero",
                moneyBill: "fa-money-bill",
                moneyBillAlt: "fa-money-bill-alt",
                moon: "fa-moon",
                motorcycle: "fa-motorcycle",
                mousePointer: "fa-mouse-pointer",
                music: "fa-music",
                napster: "fa-napster",
                neuter: "fa-neuter",
                newspaper: "fa-newspaper",
                nintendoSwitch: "fa-nintendo-switch",
                node: "fa-node",
                nodeJs: "fa-node-js",
                npm: "fa-npm",
                ns8: "fa-ns8",
                nutritionix: "fa-nutritionix",
                objectGroup: "fa-object-group",
                objectUngroup: "fa-object-ungroup",
                octagon: "fa-octagon",
                odnoklassniki: "fa-odnoklassniki",
                odnoklassnikiSquare: "fa-odnoklassniki-square",
                openCart: "fa-opencart",
                openid: "fa-openid",
                opera: "fa-opera",
                optinMonster: "fa-optin-monster",
                osi: "fa-osi",
                outDent: "fa-outdent",
                page4: "fa-page4",
                pageLines: "fa-pagelines",
                paintBrush: "fa-paint-brush",
                palFed: "fa-palfed",
                paperPlane: "fa-paper-plane",
                paperclip: "fa-paperclip",
                paragraph: "fa-paragraph",
                paste: "fa-paste",
                patreon: "fa-patreon",
                pause: "fa-pause",
                pauseCircle: "fa-pause-circle",
                paw: "fa-paw",
                paypal: "fa-paypal",
                pen: "fa-pen",
                penAlt: "fa-pen-alt",
                penSquare: "fa-pen-square",
                pencil: "fa-pencil",
                pencilAlt: "fa-pencil-alt",
                percent: "fa-percent",
                periscope: "fa-periscope",
                phabricator: "fa-phabricator",
                phoenixFramework: "fa-phoenix-framework",
                phone: "fa-phone",
                phoneSlash: "fa-phone-slash",
                phoneSquare: "fa-phone-square",
                phoneVolume: "fa-phone-volume",
                piedPiper: "fa-pied-piper",
                piedPiperAlt: "fa-pied-piper-alt",
                piedPiperPp: "fa-pied-piper-pp",
                pinterest: "fa-pinterest",
                pinterestP: "fa-pinterest-p",
                pinterestSquare: "fa-pinterest-square",
                plane: "fa-plane",
                planeAlt: "fa-plane-alt",
                play: "fa-play",
                playCircle: "fa-play-circle",
                playstation: "fa-playstation",
                plug: "fa-plug",
                plus: "fa-plus",
                plusCircle: "fa-plus-circle",
                plusHexagon: "fa-plus-hexagon",
                plusOctagon: "fa-plus-octagon",
                plusSquare: "fa-plus-square",
                podCast: "fa-podcast",
                poo: "fa-poo",
                portrait: "fa-portrait",
                poundSign: "fa-pound-sign",
                powerOff: "fa-power-off",
                print: "fa-print",
                productHunt: "fa-product-hunt",
                pushed: "fa-pushed",
                puzzlePiece: "fa-puzzle-piece",
                python: "fa-python",
                qq: "fa-qq",
                qrcode: "fa-qrcode",
                question: "fa-question",
                questionCircle: "fa-question-circle",
                questionSquare: "fa-question-square",
                quora: "fa-quora",
                quoteLeft: "fa-quote-left",
                quoteRight: "fa-quote-right",
                random: "fa-random",
                ravelry: "fa-ravelry",
                react: "fa-react",
                rebel: "fa-rebel",
                rectangleLandscape: "fa-rectangle-landscape",
                rectanglePortrait: "fa-rectangle-portrait",
                rectangleWide: "fa-rectangle-wide",
                recycle: "fa-recycle",
                redRiver: "fa-red-river",
                reddit: "fa-reddit",
                redditAlien: "fa-reddit-alien",
                redditSquare: "fa-reddit-square",
                redo: "fa-redo",
                redoAlt: "fa-redo-alt",
                registered: "fa-registered",
                rendact: "fa-rendact",
                renren: "fa-renren",
                repeat: "fa-repeat",
                repeatL: "fa-repeat-1",
                repeatLAlt: "fa-repeat-1-alt",
                repeatAlt: "fa-repeat-alt",
                reply: "fa-reply",
                replyAll: "fa-reply-all",
                replyd: "fa-replyd",
                resolving: "fa-resolving",
                retweet: "fa-retweet",
                retweetAlt: "fa-retweet-alt",
                road: "fa-road",
                rocket: "fa-rocket",
                rocketchat: "fa-rocketchat",
                rockrms: "fa-rockrms",
                rss: "fa-rss",
                rssSquare: "fa-rss-square",
                rubleSign: "fa-ruble-sign",
                rupeeSign: "fa-rupee-sign",
                safari: "fa-safari",
                sass: "fa-sass",
                save: "fa-save",
                schlix: "fa-schlix",
                scribd: "fa-scribd",
                scrubber: "fa-scrubber",
                search: "fa-search",
                searchMinus: "fa-search-minus",
                searchPlus: "fa-search-plus",
                searchengin: "fa-searchengin",
                sellcast: "fa-sellcast",
                sellsy: "fa-sellsy",
                server: "fa-server",
                servicestack: "fa-servicestack",
                share: "fa-share",
                shareAll: "fa-share-all",
                shareAlt: "fa-share-alt",
                shareAltSquare: "fa-share-alt-square",
                shareSquare: "fa-share-square",
                shekelSign: "fa-shekel-sign",
                shield: "fa-shield",
                shieldAlt: "fa-shield-alt",
                shieldCheck: "fa-shield-check",
                ship: "fa-ship",
                shirtsinbulk: "fa-shirtsinbulk",
                shoppingBag: "fa-shopping-bag",
                shoppingBasket: "fa-shopping-basket",
                shoppingCart: "fa-shopping-cart",
                shower: "fa-shower",
                signIn: "fa-sign-in",
                signInAlt: "fa-sign-in-alt",
                signLanguage: "fa-sign-language",
                signOut: "fa-sign-out",
                signOutAlt: "fa-sign-out-alt",
                signal: "fa-signal",
                simplybuilt: "fa-simplybuilt",
                sistrix: "fa-sistrix",
                sitemap: "fa-sitemap",
                skyatlas: "fa-skyatlas",
                skype: "fa-skype",
                slack: "fa-slack",
                slackHash: "fa-slack-hash",
                slidersH: "fa-sliders-h",
                slidersHSquare: "fa-sliders-h-square",
                slidersV: "fa-sliders-v",
                slidersVSquare: "fa-sliders-v-square",
                slideshare: "fa-slideshare",
                smile: "fa-smile",
                snapChat: "fa-snapchat",
                snapChatGhost: "fa-snapchat-ghost",
                snapChatSquare: "fa-snapchat-square",
                snowflake: "fa-snowflake",
                sort: "fa-sort",
                sortAlphaDown: "fa-sort-alpha-down",
                sortAlphaUp: "fa-sort-alpha-up",
                sortAmountDown: "fa-sort-amount-down",
                sortAmountUp: "fa-sort-amount-up",
                sortDown: "fa-sort-down",
                sortNumericDown: "fa-sort-numeric-down",
                sortNumericUp: "fa-sort-numeric-up",
                sortUp: "fa-sort-up",
                soundCloud: "fa-soundcloud",
                spaceShuttle: "fa-space-shuttle",
                spade: "fa-spade",
                speakap: "fa-speakap",
                spinner: "fa-spinner",
                spinnerThird: "fa-spinner-third",
                spotify: "fa-spotify",
                square: "fa-square",
                stackExchange: "fa-stack-exchange",
                stackOverflow: "fa-stack-overflow",
                star: "fa-star",
                starExclamation: "fa-star-exclamation",
                starHalf: "fa-star-half",
                stayLinked: "fa-staylinked",
                steam: "fa-steam",
                steamSquare: "fa-steam-square",
                steamSymbol: "fa-steam-symbol",
                stepBackward: "fa-step-backward",
                stepForward: "fa-step-forward",
                stethoscope: "fa-stethoscope",
                stickerMule: "fa-sticker-mule",
                stickyNote: "fa-sticky-note",
                stop: "fa-stop",
                stopCircle: "fa-stop-circle",
                stopwatch: "fa-stopwatch",
                strava: "fa-strava",
                streetView: "fa-street-view",
                strikeThrough: "fa-strikethrough",
                stripe: "fa-stripe",
                stripeS: "fa-stripe-s",
                studiovinari: "fa-studiovinari",
                stumbleUpon: "fa-stumbleupon",
                stumbleUponCircle: "fa-stumbleupon-circle",
                subscript: "fa-subscript",
                subway: "fa-subway",
                suitcase: "fa-suitcase",
                sun: "fa-sun",
                superpowers: "fa-superpowers",
                superscript: "fa-superscript",
                supple: "fa-supple",
                sync: "fa-sync",
                syncAlt: "fa-sync-alt",
                table: "fa-table",
                tablet: "fa-tablet",
                tabletAlt: "fa-tablet-alt",
                tabletAndroid: "fa-tablet-android",
                tabletAndroidAlt: "fa-tablet-android-alt",
                tachometer: "fa-tachometer",
                tachometerAlt: "fa-tachometer-alt",
                tag: "fa-tag",
                tags: "fa-tags",
                tasks: "fa-tasks",
                taxi: "fa-taxi",
                telegram: "fa-telegram",
                telegramPlane: "fa-telegram-plane",
                tencentWeibo: "fa-tencent-weibo",
                terminal: "fa-terminal",
                textHeight: "fa-text-height",
                textWidth: "fa-text-width",
                th: "fa-th",
                thLarge: "fa-th-large",
                thList: "fa-th-list",
                themeisle: "fa-themeisle",
                thermometerEmpty: "fa-thermometer-empty",
                thermometerFull: "fa-thermometer-full",
                thermometerHalf: "fa-thermometer-half",
                thermometerQuarter: "fa-thermometer-quarter",
                thermometerThreeQuarters: "fa-thermometer-three-quarters",
                thumbsDown: "fa-thumbs-down",
                thumbsUp: "fa-thumbs-up",
                thumbtack: "fa-thumbtack",
                ticket: "fa-ticket",
                ticketAlt: "fa-ticket-alt",
                times: "fa-times",
                timesCircle: "fa-times-circle",
                timesHexagon: "fa-times-hexagon",
                timesOctagon: "fa-times-octagon",
                timesSquare: "fa-times-square",
                tint: "fa-tint",
                toggleOff: "fa-toggle-off",
                toggleOn: "fa-toggle-on",
                trademark: "fa-trademark",
                train: "fa-train",
                transGender: "fa-transgender",
                transGenderAlt: "fa-transgender-alt",
                trash: "fa-trash",
                trashAlt: "fa-trash-alt",
                tree: "fa-tree",
                treeAlt: "fa-tree-alt",
                trello: "fa-trello",
                triangle: "fa-triangle",
                tripAdvisor: "fa-tripadvisor",
                trophy: "fa-trophy",
                trophyAlt: "fa-trophy-alt",
                truck: "fa-truck",
                tty: "fa-tty",
                tumblr: "fa-tumblr",
                tumblrSquare: "fa-tumblr-square",
                tv: "fa-tv",
                tvRetro: "fa-tv-retro",
                twitch: "fa-twitch",
                twitter: "fa-twitter",
                twitterSquare: "fa-twitter-square",
                typo3: "fa-typo3",
                uber: "fa-uber",
                uikit: "fa-uikit",
                umbrella: "fa-umbrella",
                underline: "fa-underline",
                undo: "fa-undo",
                undoAlt: "fa-undo-alt",
                uniRegistry: "fa-uniregistry",
                universalAccess: "fa-universal-access",
                university: "fa-university",
                unlink: "fa-unlink",
                unlock: "fa-unlock",
                unlockAlt: "fa-unlock-alt",
                untappd: "fa-untappd",
                upload: "fa-upload",
                usb: "fa-usb",
                usdCircle: "fa-usd-circle",
                usdSquare: "fa-usd-square",
                user: "fa-user",
                userAlt: "fa-user-alt",
                userCircle: "fa-user-circle",
                userMd: "fa-user-md",
                userPlus: "fa-user-plus",
                userSecret: "fa-user-secret",
                userTimes: "fa-user-times",
                users: "fa-users",
                ussunnah: "fa-ussunnah",
                utensilFork: "fa-utensil-fork",
                utensilKnife: "fa-utensil-knife",
                utensilSpoon: "fa-utensil-spoon",
                utensils: "fa-utensils",
                utensilsAlt: "fa-utensils-alt",
                vaadin: "fa-vaadin",
                venus: "fa-venus",
                venusDouble: "fa-venus-double",
                venusMars: "fa-venus-mars",
                viaCoin: "fa-viacoin",
                viadeo: "fa-viadeo",
                viadeoSquare: "fa-viadeo-square",
                viber: "fa-viber",
                video: "fa-video",
                vimeo: "fa-vimeo",
                vimeoSquare: "fa-vimeo-square",
                vimeoV: "fa-vimeo-v",
                vine: "fa-vine",
                vk: "fa-vk",
                vnv: "fa-vnv",
                volumeDown: "fa-volume-down",
                volumeMute: "fa-volume-mute",
                volumeOff: "fa-volume-off",
                volumeUp: "fa-volume-up",
                vuejs: "fa-vuejs",
                watch: "fa-watch",
                weibo: "fa-weibo",
                weixin: "fa-weixin",
                whatsapp: "fa-whatsapp",
                whatsappSquare: "fa-whatsapp-square",
                wheelchair: "fa-wheelchair",
                whmcs: "fa-whmcs",
                wifi: "fa-wifi",
                wikipediaW: "fa-wikipedia-w",
                window: "fa-window",
                windowAlt: "fa-window-alt",
                windowClose: "fa-window-close",
                windowMaximize: "fa-window-maximize",
                windowMinimize: "fa-window-minimize",
                windowRestore: "fa-window-restore",
                windows: "fa-windows",
                wonSign: "fa-won-sign",
                wordpress: "fa-wordpress",
                wordpressSimple: "fa-wordpress-simple",
                wpbeginner: "fa-wpbeginner",
                wpexplorer: "fa-wpexplorer",
                wpforms: "fa-wpforms",
                wrench: "fa-wrench",
                xbox: "fa-xbox",
                xing: "fa-xing",
                xingSquare: "fa-xing-square",
                yCombinator: "fa-y-combinator",
                yahoo: "fa-yahoo",
                yandex: "fa-yandex",
                yandexInternational: "fa-yandex-international",
                yelp: "fa-yelp",
                yenSign: "fa-yen-sign",
                yoast: "fa-yoast",
                youtube: "fa-youtube",
                youtubeSquare: "fa-youtube-square"
            };
        },
        enumerable: true,
        configurable: true
    });
    return FontAwesomeFactory;
}());

var Reporter = /** @class */ (function () {
    function Reporter() {
    }
    /**
     * Generates csv and jtl files based on provided browser logs
     * @param {any} browserLogs
     * @param {any} resolve
     */
    Reporter.performanceOnComplete = function (browserInstance, resolve) {
        return __awaiter(this, void 0, void 0, function () {
            var delimiter, endOfLine, requestsString, timeString, requestsSent, responseReceived, browserLogs, _i, _a, key, browserLog, message, totalTimes, requestNumbers, xhrData, jtlData, jtlEndpointData, _b, _c, id, requestWalltime, currentType, currentPage, currentUrl, elapsedTime, responseCode, responseStatus, isSuccess, endpoint, fractionDigits, csvData, totalRequests, totalTime, totalAverage, _d, _e, page, _f, _g, type, typeRequestNumber, typeTime, _h, _j, endpoint;
            return __generator(this, function (_k) {
                switch (_k.label) {
                    case 0:
                        _k.trys.push([0, , 3, 4]);
                        if (!process.env.BUILD_NUMBER) return [3 /*break*/, 2];
                        delimiter = ",";
                        endOfLine = "\r\n";
                        requestsString = "requests";
                        timeString = "time";
                        requestsSent = {};
                        responseReceived = {};
                        return [4 /*yield*/, browserInstance.manage().logs().get("performance")];
                    case 1:
                        browserLogs = _k.sent();
                        for (_i = 0, _a = Object.keys(browserLogs); _i < _a.length; _i++) {
                            key = _a[_i];
                            browserLog = browserLogs[key];
                            message = JSON.parse(browserLog.message).message;
                            if (message.method === "Network.requestWillBeSent") {
                                requestsSent[message.params.requestId] = message;
                            }
                            else if (message.method === "Network.responseReceived") {
                                responseReceived[message.params.requestId] = message;
                            }
                        }
                        totalTimes = [];
                        requestNumbers = [];
                        xhrData = [];
                        jtlData = "timeStamp,elapsed,label,responseCode,responseMessage,success,failureMessage,bytes,sentBytes,allThreads,URL";
                        jtlEndpointData = jtlData;
                        for (_b = 0, _c = Object.keys(responseReceived); _b < _c.length; _b++) {
                            id = _c[_b];
                            if (requestsSent[id]) {
                                requestWalltime = requestsSent[id].params.wallTime.toString().split(".").join("");
                                currentType = responseReceived[id].params.type;
                                currentPage = this.quoteString(requestsSent[id].params.documentURL);
                                currentUrl = requestsSent[id].params.request.url;
                                elapsedTime = Math.round((responseReceived[id].params.timestamp - requestsSent[id].params.timestamp) * 1000);
                                responseCode = responseReceived[id].params.response.status;
                                responseStatus = this.quoteString(responseReceived[id].params.response.statusText);
                                isSuccess = false;
                                if (responseCode >= 200 && responseCode <= 209) {
                                    isSuccess = true;
                                }
                                if (!totalTimes[currentPage]) {
                                    totalTimes[currentPage] = [];
                                    requestNumbers[currentPage] = [];
                                }
                                if (totalTimes[currentPage][currentType]) {
                                    totalTimes[currentPage][currentType] += elapsedTime;
                                    requestNumbers[currentPage][currentType]++;
                                }
                                else {
                                    totalTimes[currentPage][currentType] = elapsedTime;
                                    requestNumbers[currentPage][currentType] = 1;
                                }
                                if (currentType === "XHR") {
                                    endpoint = this.quoteString(currentUrl.split("?")[0]);
                                    if (!xhrData[endpoint]) {
                                        xhrData[endpoint] = {};
                                        xhrData[endpoint][requestsString] = 0;
                                        xhrData[endpoint][timeString] = 0;
                                    }
                                    xhrData[endpoint][requestsString]++;
                                    xhrData[endpoint][timeString] += elapsedTime;
                                    jtlEndpointData += endOfLine;
                                    jtlEndpointData += [
                                        requestWalltime,
                                        elapsedTime,
                                        endpoint,
                                        responseCode,
                                        responseStatus,
                                        isSuccess,
                                        responseStatus,
                                        0,
                                        0,
                                        0,
                                        currentPage
                                    ].join(delimiter);
                                }
                                if (currentUrl.includes("data:image")) {
                                    currentUrl = currentUrl.split(";")[0];
                                }
                                jtlData += endOfLine;
                                jtlData += [
                                    requestWalltime,
                                    elapsedTime,
                                    currentPage,
                                    responseCode,
                                    responseStatus,
                                    isSuccess,
                                    responseStatus,
                                    0,
                                    0,
                                    0,
                                    this.quoteString(currentUrl)
                                ].join(delimiter);
                            }
                        }
                        fractionDigits = 3;
                        csvData = ",Requests,Total time,Average time";
                        totalRequests = 0;
                        totalTime = 0;
                        totalAverage = 0;
                        for (_d = 0, _e = Object.keys(totalTimes); _d < _e.length; _d++) {
                            page = _e[_d];
                            console.log(page);
                            csvData += endOfLine + page;
                            for (_f = 0, _g = Object.keys(totalTimes[page]); _f < _g.length; _f++) {
                                type = _g[_f];
                                typeRequestNumber = requestNumbers[page][type];
                                typeTime = totalTimes[page][type];
                                totalRequests += typeRequestNumber;
                                totalTime += typeTime;
                                totalAverage += typeTime / typeRequestNumber;
                                console.log(type + "\n            requests: " + typeRequestNumber + "\n            total time: " + typeTime.toFixed(fractionDigits) + "\n            average time: " + (typeTime / typeRequestNumber).toFixed(fractionDigits));
                                csvData += endOfLine;
                                csvData += [
                                    type,
                                    typeRequestNumber,
                                    typeTime.toFixed(fractionDigits),
                                    (typeTime / typeRequestNumber).toFixed(fractionDigits)
                                ].join(delimiter);
                            }
                        }
                        console.log("Total\n            requests: " + totalRequests + "\n            total time: " + totalTime.toFixed(fractionDigits) + "\n            average time: " + totalAverage.toFixed(fractionDigits));
                        csvData += endOfLine;
                        csvData += [
                            "Total",
                            totalRequests,
                            totalTime.toFixed(fractionDigits),
                            totalAverage.toFixed(fractionDigits)
                        ].join(delimiter);
                        csvData += endOfLine;
                        for (_h = 0, _j = Object.keys(xhrData); _h < _j.length; _h++) {
                            endpoint = _j[_h];
                            csvData += endOfLine;
                            csvData += [
                                endpoint,
                                xhrData[endpoint][requestsString],
                                xhrData[endpoint][timeString].toFixed(fractionDigits),
                                (xhrData[endpoint][timeString] / xhrData[endpoint][requestsString]).toFixed(fractionDigits)
                            ].join(delimiter);
                        }
                        this.saveFile("performance.csv", csvData);
                        this.saveFile("pages.jtl", jtlData);
                        this.saveFile("endpoints.jtl", jtlEndpointData);
                        _k.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        resolve();
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    Reporter.saveFile = function (filePath, fileData) {
        var fs = require("fs");
        fs.writeFile(filePath, fileData, function (error) {
            if (error) {
                console.error(error);
                return error;
            }
        });
    };
    Reporter.quoteString = function (csvString) {
        return "\"" + csvString + "\"";
    };
    return Reporter;
}());

// Import here Polyfills if needed. Recommended core-js (npm i -D core-js)
// import "core-js/fn/array.find"
// ...
process.setMaxListeners(0);

exports.ButtonUtilitiesFactory = ButtonUtilitiesFactory;
exports.InputUtilitiesFactory = InputUtilitiesFactory;
exports.SectionUtilitiesFactory = SectionUtilitiesFactory;
exports.CalendarUtilitiesFactory = CalendarUtilitiesFactory;
exports.DropdownUtilitiesFactory = DropdownUtilitiesFactory;
exports.BrowserUtilitiesFactory = BrowserUtilitiesFactory;
exports.SidebarMenuComponentSelectorsFactory = SidebarMenuComponentSelectorsFactory;
exports.ConstantsFactory = ConstantsFactory;
exports.BreadcrumbsComponentSelectorsFactory = BreadcrumbsComponentSelectorsFactory;
exports.DropdownComponentSelectorsFactory = DropdownComponentSelectorsFactory;
exports.TabComponentSelectorsFactory = TabComponentSelectorsFactory;
exports.TooltipComponentSelectorsFactory = TooltipComponentSelectorsFactory;
exports.LoaderComponentSelectorsFactory = LoaderComponentSelectorsFactory;
exports.TextBoxComponentSelectorsFactory = TextBoxComponentSelectorsFactory;
exports.ButtonComponentSelectorsFactory = ButtonComponentSelectorsFactory;
exports.CalendarComponentSelectorsFactory = CalendarComponentSelectorsFactory;
exports.ToastComponentSelectorsFactory = ToastComponentSelectorsFactory;
exports.ButtonHelperFactory = ButtonHelperFactory;
exports.PageHelperFactory = PageHelperFactory;
exports.WaitHelperFactory = WaitHelperFactory;
exports.RadioComponentSelectorsFactory = RadioComponentSelectorsFactory;
exports.LabelComponentSelectorsFactory = LabelComponentSelectorsFactory;
exports.GridComponentSelectorsFactory = GridComponentSelectorsFactory;
exports.HtmlHelperFactory = HtmlHelperFactory;
exports.ComponentHelpersFactory = ComponentHelpersFactory;
exports.CheckboxComponentSelectorsFactory = CheckboxComponentSelectorsFactory;
exports.SectionComponentSelectorsFactory = SectionComponentSelectorsFactory;
exports.ModelComponentSelectorsFactory = ModelComponentSelectorsFactory;
exports.CommonLabelFactory = CommonLabelFactory;
exports.FontAwesomeFactory = FontAwesomeFactory;
exports.Reporter = Reporter;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=protractor-automation-helper.umd.js.map
