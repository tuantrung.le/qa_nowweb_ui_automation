export declare class CheckboxComponentSelectorsFactory {
    static readonly selector: string;
    static readonly checkboxBoxIcon: string;
    static getControlXpath(label: string, isContains?: boolean): string;
    static getCheckboxByFormControlNameXpath(name: string): string;
}
