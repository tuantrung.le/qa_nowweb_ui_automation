export declare class GridComponentSelectorsFactory {
    static readonly selector: string;
    static getCellByText(text: string, isContains?: boolean): string;
    static getColumnItemXPath(columnName: string, listName: string, isContains?: boolean): string;
    static getSelectItemCheckBoxXPath(dataHeaderName: string, dataHeaderItemName: string): string;
    static columnItemButtonXPath(columnName: string, listName: string, buttonName: string, isContainsInButtonName?: boolean): string;
    static getDropDownOptionsXPath(dropDownName: string): string;
    static getDropDownOptionXPath(dropDownName: string, optionName: string): string;
}
