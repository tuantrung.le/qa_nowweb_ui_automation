export declare class LabelComponentSelectorsFactory {
    static readonly selector: string;
    static getInfoIconXPath(labelName: string): string;
}
