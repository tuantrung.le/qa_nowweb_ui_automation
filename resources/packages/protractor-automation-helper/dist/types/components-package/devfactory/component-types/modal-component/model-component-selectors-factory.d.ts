export declare class ModelComponentSelectorsFactory {
    static readonly selector: string;
    static readonly modelPopupBodyXpath: string;
    static getModelPopupXpath(insidePopup: boolean): string;
}
