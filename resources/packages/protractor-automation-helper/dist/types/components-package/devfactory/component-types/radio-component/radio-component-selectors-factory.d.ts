export declare class RadioComponentSelectorsFactory {
    static readonly selector: string;
    static getDfOptionsByTextXPath(text: string, isContains?: boolean): string;
}
