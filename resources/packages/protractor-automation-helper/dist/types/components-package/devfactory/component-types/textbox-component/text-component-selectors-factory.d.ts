export declare class TextBoxComponentSelectorsFactory {
    static readonly selector: string;
    static getTextBoxByPlaceholderXpath(placeholder: string, insidePopup?: boolean): string;
    static getErrorMessageInPopUp(formControlName: string): string;
    static getTextBoxByLabelXpath(label: string, insidePopup?: boolean): string;
    static getTextBoxErrorLabelXpath(label: string, insidePopup?: boolean): string;
    static getXpathForForTextboxFollowingSiblingByLabel(label: string, insidePopup?: boolean): string;
    static getTextBoxByFormControlNameXpath(formControlName: string, insidePopup?: boolean): string;
}
