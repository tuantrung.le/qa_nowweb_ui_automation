export declare class ToastComponentSelectorsFactory {
    static readonly selector: string;
    static readonly toastHeaderXPath: string;
    static readonly toastBodyXPath: string;
    static readonly toastContentXPath: string;
}
