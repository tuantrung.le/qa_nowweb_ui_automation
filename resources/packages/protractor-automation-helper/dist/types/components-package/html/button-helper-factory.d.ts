export declare class ButtonHelperFactory {
    static getButtonByExactTextXPath(text: string, isContains?: boolean): string;
}
