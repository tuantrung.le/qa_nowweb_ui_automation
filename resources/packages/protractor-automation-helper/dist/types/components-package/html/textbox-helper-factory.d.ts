import { ProtractorBrowser } from "protractor";
import { WebdriverBy } from "protractor/built/locators";
export declare class TextBoxHelperFactory {
    private _browser;
    private _by;
    private waitHelperFactory;
    private pageHelperFactory;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy);
    /**
     * Clears the existing text from an input elements
     * @param {any} locator
     */
    clearText(locator: any): Promise<void>;
    /**
     * Send Keys to an input elements once it becomes available
     * @param {any} locator for element
     * @param {string} value to be sent
     * @param {boolean} sendEnter for sending an enter key
     */
    sendKeys(locator: any, value: string, sendEnter?: boolean): Promise<void>;
    /**
     * Checks whether an input box has particular value or not
     * @param {any} locator
     * @param {string} text
     * @returns {PromiseLike<boolean> | Promise<boolean> | Q.Promise<any> | promise.Promise<any> | Q.IPromise<any>}
     */
    hasValue(locator: any, text: string): Promise<boolean>;
}
