export declare class HtmlHelperFactory {
    static readonly attributes: {
        accept: string;
        acceptCharset: string;
        accessKey: string;
        action: string;
        align: string;
        alt: string;
        async: string;
        autoComplete: string;
        autoFocus: string;
        autoPlay: string;
        bgColor: string;
        border: string;
        buffered: string;
        challenge: string;
        charset: string;
        checked: string;
        cite: string;
        class: string;
        code: string;
        codebase: string;
        color: string;
        cols: string;
        colspan: string;
        content: string;
        contentEditable: string;
        contextMenu: string;
        controls: string;
        coords: string;
        crossOrigin: string;
        data: string;
        datetime: string;
        defaultt: string;
        defer: string;
        dir: string;
        dirName: string;
        disabled: string;
        download: string;
        draggable: string;
        dropZone: string;
        enctype: string;
        for: string;
        form: string;
        formControlName: string;
        formAction: string;
        headers: string;
        height: string;
        hidden: string;
        high: string;
        href: string;
        hrefLang: string;
        httpEquiv: string;
        icon: string;
        id: string;
        integrity: string;
        isMap: string;
        itemProp: string;
        keyType: string;
        kind: string;
        label: string;
        lang: string;
        language: string;
        list: string;
        loop: string;
        low: string;
        manifest: string;
        max: string;
        maxLength: string;
        minLength: string;
        media: string;
        method: string;
        min: string;
        multiple: string;
        muted: string;
        name: string;
        novalidate: string;
        open: string;
        optimum: string;
        pattern: string;
        ping: string;
        placeholder: string;
        poster: string;
        preload: string;
        radioGroup: string;
        readonly: string;
        rel: string;
        required: string;
        reversed: string;
        rows: string;
        rowSpan: string;
        sandbox: string;
        scope: string;
        scoped: string;
        seamless: string;
        selected: string;
        shape: string;
        size: string;
        sizes: string;
        slot: string;
        span: string;
        spellCheck: string;
        src: string;
        srcDoc: string;
        srcLang: string;
        srcset: string;
        start: string;
        step: string;
        style: string;
        summary: string;
        tabIndex: string;
        target: string;
        title: string;
        type: string;
        useMap: string;
        value: string;
        width: string;
        wrap: string;
    };
}
