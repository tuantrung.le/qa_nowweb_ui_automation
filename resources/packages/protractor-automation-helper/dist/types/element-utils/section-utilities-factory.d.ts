import { ProtractorBrowser, WebElementPromise } from "protractor";
import { WebdriverBy } from "protractor/built/locators";
export declare class SectionUtilitiesFactory {
    private _browser;
    private _by;
    constructor(_browser: ProtractorBrowser, _by: WebdriverBy);
    getSectionById(elementId: string): WebElementPromise;
}
