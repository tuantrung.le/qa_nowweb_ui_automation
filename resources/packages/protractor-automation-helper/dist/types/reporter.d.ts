export declare class Reporter {
    /**
     * Generates csv and jtl files based on provided browser logs
     * @param {any} browserLogs
     * @param {any} resolve
     */
    static performanceOnComplete(browserInstance: any, resolve: any): Promise<void>;
    private static saveFile;
    private static quoteString;
}
